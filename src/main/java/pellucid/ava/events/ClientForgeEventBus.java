package pellucid.ava.events;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.*;
import pellucid.ava.items.miscs.MeleeWeapon;
import pellucid.ava.misc.config.AVAClientConfig;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.meleemodels.field_knife.FieldKnifeModel;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;
import pellucid.ava.misc.renderers.models.fg42.FG42Model;
import pellucid.ava.misc.renderers.models.fn_fnc.FnfncModel;
import pellucid.ava.misc.renderers.models.gm94.GM94Model;
import pellucid.ava.misc.renderers.models.mk18.Mk18Model;
import pellucid.ava.misc.renderers.models.mk20.MK20Model;
import pellucid.ava.misc.renderers.models.mosin_nagant.MosinNagantModel;
import pellucid.ava.misc.renderers.models.mp5k.Mp5kModel;
import pellucid.ava.misc.renderers.models.mp5sd5.Mp5sd5Model;
import pellucid.ava.misc.renderers.models.p226.P226Model;
import pellucid.ava.misc.renderers.models.python357.Python357Model;
import pellucid.ava.misc.renderers.models.remington870.Reminton870Model;
import pellucid.ava.misc.renderers.models.sr_25.Sr25Model;
import pellucid.ava.misc.renderers.models.x95r.X95RModel;
import pellucid.ava.misc.renderers.models.xm8.Xm8Model;

import java.util.ArrayList;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class ClientForgeEventBus extends AVAClientEvent
{
    @SubscribeEvent
    public static void onHandRender(RenderHandEvent event)
    {
        ClientPlayerEntity player = Minecraft.getInstance().player;
        if (player != null && player.isAlive())
        {
            ItemStack itemStack = player.getHeldItemMainhand();
            if (itemStack.getItem() instanceof AVAItemGun)
            {
                CompoundNBT compound = itemStack.getOrCreateTag();
                if (!isAiming(player))
                {
                    //Run animations
                    MatrixStack stack = event.getMatrixStack();
                    stack.push();
                    stack.translate(0.0D, -0.46F, -0.32F);
                    stack.rotate(Vector3f.XP.rotationDegrees(-85.0F));
                    stack.rotate(Vector3f.YP.rotationDegrees(180.0F));
                    PlayerRenderer renderer = (PlayerRenderer) Minecraft.getInstance().getRenderManager().getRenderer(player);
                    AVAItemGun master = getGun(player).getMaster();
                    int reloadTick = compound.getInt("reload");
                    int runTick = compound.getInt("run");
                    if (runTick != 0)
                    {
                        ArrayList<Animation> leftAnimations = new ArrayList<>();
                        ArrayList<Animation> rightAnimations = new ArrayList<>();
                        //tslt ->    +     /    -
                        // x   ->   left   /  right
                        // y   -> forward  / backward
                        // z   ->   down   /   up

                        //rot  ->    +     /    -
                        // x   ->lift-back/ lift-front
                        // y   ->tilt-right/ tilt-left
//                      // z   ->clockwise /    ac
                        if (master == Pistols.P226 || master == Pistols.SW1911_COLT || master == Pistols.PYTHON357)
                        {
                            leftAnimations = P226Model.LEFT_HAND_RUN_ANIMATION_FP;
                            rightAnimations = P226Model.RIGHT_HAND_RUN_ANIMATION_FP;
                        }
                        else if (master == SubmachineGuns.REMINGTON870)
                            leftAnimations = Reminton870Model.LEFT_HAND_RUN_ANIMATION_FP;
                        if (!leftAnimations.isEmpty())
                        {
                            stack.push();
                            transferStack(stack, ModifiedGunModel.getPerspectiveInBetween(leftAnimations, runTick));
                            renderer.renderLeftArm(stack, event.getBuffers(), event.getLight(), player);
                            stack.pop();
                        }
                        if (!rightAnimations.isEmpty())
                        {
                            stack.push();
                            transferStack(stack, ModifiedGunModel.getPerspectiveInBetween(rightAnimations, runTick));
                            renderer.renderRightArm(stack, event.getBuffers(), event.getLight(), player);
                            stack.pop();
                        }
                    }
                    //Idle animation
                    else if (reloadTick == 0)
                    {
                        stack.push();
                        boolean renderRightArm = false;
                        //tslt ->    +     /    -
                        // x   ->   left   /  right
                        // y   -> forward  / backward
                        // z   ->   down   /   up

                        //rot  ->    +     /    -
                        // x   ->lift-back/ lift-front
                        // y   ->tilt-right/ tilt-left
                        // z   ->clockwise /    ac
                        boolean renderLeftArm = true;
                        if (master == Pistols.PYTHON357)
                        {
                            renderLeftArm = false;
                            renderRightArm = true;
                        }
                        if (master == Pistols.P226 || master == Pistols.SW1911_COLT)
                        {
                            stack.translate(-0.4F, -0.5F, 0.025F);
                            renderRightArm = true;
                        }
                        else if (master == Rifles.MK20 || master == SubmachineGuns.MP5K)
                        {
                            stack.translate(-0.25F, -0.5F, 0.025);
                            stack.scale(0.65F, 1.0F, 1.0F);
                        }
                        else if (master == Rifles.FN_FNC)
                            stack.translate(-0.35F, -0.275F, 0.1F);
                        else if (master == Rifles.XM8)
                        {
                            stack.translate(-0.425F, -0.2F, 0.1F);
                            stack.scale(0.7F, 0.85F, 1.0F);
                        }
                        else if (master == Rifles.FG42)
                            stack.translate(-0.5F, 0, 0.175F);
                        else if (master == Snipers.MOSIN_NAGANT)
                            stack.translate(-0.5F, -0.25, 0.15F);
                        else if (master == Snipers.M24)
                            stack.translate(-0.225F, -0.35F, 0);
                        else if (master == SubmachineGuns.MP5SD5 || master == SubmachineGuns.REMINGTON870)
                            stack.translate(-0.35F, -0.275F, 0.05F);
                        else if (master == Snipers.SR_25)
                        {
                            stack.translate(-0.275F, -0.175F, -0.05F);
                            stack.scale(0.5F, 0.5F, 1.0F);
                        }
                        else if (master == SubmachineGuns.MK18)
                            stack.translate(-0.35F, -0.275F, 0.075F);
                        else
                            stack.translate(-0.5F, 0, 0.275F);
                        if (compound.getInt("fire") != 0)
                        {
                            if (master == Snipers.M24)
                                renderLeftArm = false;
                            if (master != Snipers.SR_25)
                                stack.rotate(Vector3f.XN.rotationDegrees(5.0F));
                        }
                        bobMatrixStack(stack, player);
                        stack.rotate(Vector3f.ZP.rotationDegrees(35));
                        renderLeftArm = renderLeftArm && player.getHeldItemOffhand().isEmpty();
                        if (renderLeftArm)
                            renderer.renderLeftArm(event.getMatrixStack(), event.getBuffers(), event.getLight(), player);
                        stack.pop();
                        if (renderRightArm)
                        {
                            stack.push();
                            if (master == Pistols.P226 || master == Pistols.SW1911_COLT)
                                stack.translate(0, -0.5F, 0);
                            else if (master == Pistols.PYTHON357)
                            {
                                stack.translate(0.12F, -0.7F, -0.21F);
                                stack.scale(0.75F, 0.8F, 1.0F);
                            }
                            bobMatrixStack(stack, player);
                            renderer.renderRightArm(event.getMatrixStack(), event.getBuffers(), event.getLight(), player);
                            stack.pop();
                        }
                    }
                    //Reload animation
                    else if (AVAClientConfig.FP_RELOAD_ANIMATION.get())
                    {
                        ArrayList<Animation> leftAnimations = new ArrayList<>();
                        ArrayList<Animation> rightAnimations = new ArrayList<>();
                        //tslt ->    +     /    -
                        // x   ->   left   /  right
                        // y   -> forward  / backward
                        // z   ->   down   /   up

                        //rot  ->    +     /    -
                        // x   ->lift-back/ lift-front
                        // y   ->tilt-right/ tilt-left
                        // z   ->clockwise /    ac
                        if (master == Pistols.P226 || master == Pistols.SW1911_COLT)
                            rightAnimations = P226Model.RIGHT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == Rifles.MK20)
                            leftAnimations = MK20Model.LEFT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == Rifles.FN_FNC)
                        {
                            leftAnimations = FnfncModel.LEFT_HAND_RELOAD_ANIMATION_FP;
                            rightAnimations = FnfncModel.RIGHT_HAND_RELOAD_ANIMATION_FP;
                        }
                        else if (master == Rifles.XM8)
                            leftAnimations = Xm8Model.LEFT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == Rifles.FG42)
                            rightAnimations = FG42Model.RIGHT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == Snipers.MOSIN_NAGANT)
                        {
                            leftAnimations = MosinNagantModel.LEFT_HAND_RELOAD_ANIMATION_FP;
                            rightAnimations = MosinNagantModel.RIGHT_HAND_RELOAD_ANIMATION_FP;
                        }
                        else if (master == Snipers.SR_25)
                        {
                            leftAnimations = Sr25Model.LEFT_HAND_RELOAD_ANIMATION_FP;
                            rightAnimations = Sr25Model.RIGHT_HAND_RELOAD_ANIMATION_FP;
                        }
                        else if (master == SubmachineGuns.X95R)
                            leftAnimations = X95RModel.LEFT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == SubmachineGuns.MP5SD5)
                            leftAnimations = Mp5sd5Model.LEFT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == SubmachineGuns.MK18)
                            leftAnimations = Mk18Model.LEFT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == SubmachineGuns.REMINGTON870)
                            leftAnimations = Reminton870Model.LEFT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == SubmachineGuns.MP5K)
                            leftAnimations = Mp5kModel.LEFT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == SpecialWeapons.GM94)
                            leftAnimations = GM94Model.LEFT_HAND_RELOAD_ANIMATION_FP;
                        else if (master == Pistols.PYTHON357)
                            leftAnimations = Python357Model.LEFT_HAND_RELOAD_ANIMATION_FP;
                        if (!leftAnimations.isEmpty())
                        {
                            stack.push();
                            transferStack(stack, ModifiedGunModel.getPerspectiveInBetween(leftAnimations, reloadTick));
                            renderer.renderLeftArm(stack, event.getBuffers(), event.getLight(), player);
                            stack.pop();
                        }
                        if (!rightAnimations.isEmpty())
                        {
                            stack.push();
                            transferStack(stack, ModifiedGunModel.getPerspectiveInBetween(rightAnimations, reloadTick));
                            renderer.renderRightArm(stack, event.getBuffers(), event.getLight(), player);
                            stack.pop();
                        }
                    }
                    stack.pop();
                }
            }
            else if (itemStack.getItem() instanceof MeleeWeapon)
            {
                CompoundNBT compound = itemStack.getOrCreateTag();
                if (!isAiming(player))
                {
                    //Run animations
                    MatrixStack stack = event.getMatrixStack();
                    stack.push();
                    stack.translate(0.0D, -0.46F, -0.32F);
                    stack.rotate(Vector3f.XP.rotationDegrees(-85.0F));
                    stack.rotate(Vector3f.YP.rotationDegrees(180.0F));
                    PlayerRenderer renderer = (PlayerRenderer) Minecraft.getInstance().getRenderManager().getRenderer(player);
                    int attackTickL = compound.getInt("meleeL");
                    int attackTickR = compound.getInt("meleeR");
                    //Attack animation
                    if (attackTickL > 0 || attackTickR > 0)
                    {
                        boolean left = attackTickL > 0;
                        ArrayList<Animation> leftAnimations = new ArrayList<>();
                        ArrayList<Animation> rightAnimations = new ArrayList<>();
                        //tslt ->    +     /    -
                        // x   ->   left   /  right
                        // y   -> forward  / backward
                        // z   ->   down   /   up

                        //rot  ->    +     /    -
                        // x   ->lift-back/ lift-front
                        // y   ->tilt-right/ tilt-left
                        // z   ->clockwise /    ac
                        if (itemStack.getItem() == MeleeWeapons.FIELD_KNIFE)
                            rightAnimations = left ? FieldKnifeModel.LEFT_HAND_ANIMATION : FieldKnifeModel.RIGHT_HAND_ANIMATION;
                        if (!leftAnimations.isEmpty())
                        {
                            stack.push();
                            transferStack(stack, ModifiedGunModel.getPerspectiveInBetween(leftAnimations, left ? attackTickL : attackTickR));
                            renderer.renderLeftArm(stack, event.getBuffers(), event.getLight(), player);
                            stack.pop();
                        }
                        else if (!rightAnimations.isEmpty())
                        {
                            stack.push();
                            transferStack(stack, ModifiedGunModel.getPerspectiveInBetween(rightAnimations, left ? attackTickL : attackTickR));
                            renderer.renderRightArm(stack, event.getBuffers(), event.getLight(), player);
                            stack.pop();
                        }
                    }
                    else
                    {
                        boolean renderLeftArm = false;
                        boolean renderRightArm = true;
                        if (itemStack.getItem() == MeleeWeapons.FIELD_KNIFE)
                            transferStack(stack, FieldKnifeModel.RIGHT_HAND_ORIGINAL);
                        if (renderRightArm)
                        {
                            stack.push();
                            bobMatrixStack(stack, player);
                            renderer.renderRightArm(event.getMatrixStack(), event.getBuffers(), event.getLight(), player);
                            stack.pop();
                        }
                    }
                    stack.pop();
                }
            }
        }
    }

    private static ArrayList<Animation> test1()
    {
                                            //tslt ->    +     /    -
                                            // x   ->   left   /  right
                                            // y   -> forward  / backward
                                            // z   ->   down   /   up

                                            //rot  ->    +     /    -
                                            // x   ->lift-back/ lift-front
                                            // y   ->tilt-right/ tilt-left
                                            // z   ->clockwise /    ac
        //        Perspective RIGHT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 0}, new float[]{0, 0, 0}, new float[]{1, 1, 1});
        Perspective RIGHT_HAND_RELOADING_FP = new Perspective(new float[]{0.0F, 0, 25.0F}, new float[]{-0.15F, -0.6F, 0.15F}, new float[]{1, 1, 1});
        Perspective RIGHT_HAND_RELOADING_FP_2 = new Perspective(new float[]{0.0F, 0, 25.0F}, new float[]{-0.45F, -0.35F, 0}, new float[]{1, 1, 1});
        Perspective RIGHT_HAND_RELOADING_FP_3 = new Perspective(new float[]{0.0F, 0, 25.0F}, new float[]{-0.2F, -0.575F, -0.075F}, new float[]{0.85F, 0.85F, 0.85F});
        Perspective RIGHT_HAND_RELOADING_FP_4 = new Perspective(new float[]{-10.0F, -10, 40}, new float[]{-0.05F, -0.575F, -0.1F}, new float[]{0.7F, 0.7F, 1});
        Perspective RIGHT_HAND_RELOADING_FP_5 = new Perspective(new float[]{-10.0F, -10, 40}, new float[]{-0.05F, -0.575F, 0.025F}, new float[]{0.7F, 0.7F, 1});
        return new ArrayList<Animation>() {{
            add(new Animation(0, RIGHT_HAND_RELOADING_FP));
            add(new Animation(3, RIGHT_HAND_RELOADING_FP_2));
            add(new Animation(4, RIGHT_HAND_RELOADING_FP_2));
            add(new Animation(7, RIGHT_HAND_RELOADING_FP));
            add(new Animation(28, RIGHT_HAND_RELOADING_FP));
            add(new Animation(30, RIGHT_HAND_RELOADING_FP_3));
            add(new Animation(38, RIGHT_HAND_RELOADING_FP_3));
            add(new Animation(40, RIGHT_HAND_RELOADING_FP));
            add(new Animation(53, RIGHT_HAND_RELOADING_FP));
            add(new Animation(56, RIGHT_HAND_RELOADING_FP_4));
            add(new Animation(63, RIGHT_HAND_RELOADING_FP_5));
            add(new Animation(70, RIGHT_HAND_RELOADING_FP));
        }};
    }

    private static ArrayList<Animation> test2()
    {
                                            //tslt ->    +     /    -
                                            // x   ->   left   /  right
                                            // y   -> forward  / backward
                                            // z   ->   down   /   up

                                            //rot  ->    +     /    -
                                            // x   ->lift-back/ lift-front
                                            // y   ->tilt-right/ tilt-left
                                            // z   ->clockwise /    ac
        Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{0, 0, 35.0F}, new float[]{0.2F, -1.0F, -0.2F}, new float[]{0.45F, 1, 0.45F});
        Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 35.0F}, new float[]{0.15F, -0.65F, -0.25F}, new float[]{0.45F, 1, 0.45F});
        Perspective LEFT_HAND_RELOADING_2_FP = new Perspective(new float[]{0, 0, 35.0F}, new float[]{0.05F, -0.65F, -0.2F}, new float[]{0.45F, 1, 0.45F});
        return new ArrayList<Animation>() {{
            add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
            add(new Animation(4, LEFT_HAND_RELOADING_FP));
            add(new Animation(12, LEFT_HAND_RELOADING_2_FP));
            add(new Animation(14, LEFT_HAND_ORIGINAL_FP));
            add(new Animation(16, LEFT_HAND_ORIGINAL_FP));
            add(new Animation(18, LEFT_HAND_RELOADING_FP));
            add(new Animation(26, LEFT_HAND_RELOADING_2_FP));
            add(new Animation(30, LEFT_HAND_ORIGINAL_FP));
        }};
    }

    private static void bobMatrixStack(MatrixStack stack, PlayerEntity player)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        ModifiedGunModel.bob(rotation, translation, player.ticksExisted);
        transferStack(stack, new Perspective(new Vector3f(0.0F, 0.0F, 0.0F), new Vector3f(translation.getX() * -0.05F, translation.getZ() * -0.05F, translation.getY() * -0.075F), new Vector3f(1.0F, 1.0F, 1.0F)));
        if (!player.onGround && AVAClientConfig.FP_JUMP_ANIMATION.get())
            stack.translate(0.0F, 0.0F, -0.1F);
    }

    private static void rotateStack(MatrixStack stack, float[] degree)
    {
        stack.rotate(Vector3f.XP.rotationDegrees(degree[0]));
        stack.rotate(Vector3f.YP.rotationDegrees(degree[1]));
        stack.rotate(Vector3f.ZP.rotationDegrees(degree[2]));
    }

    private static void transferStack(MatrixStack stack, Perspective perspective3f)
    {
        stack.translate(perspective3f.translation.getX(), perspective3f.translation.getY(), perspective3f.translation.getZ());
        rotateStack(stack, perspective3f.getRotation());
        stack.scale(perspective3f.scale.getX(), perspective3f.scale.getY(), perspective3f.scale.getZ());
    }
}
