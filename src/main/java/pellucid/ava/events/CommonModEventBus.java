package pellucid.ava.events;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.boost_block.BoostTileEntity;
import pellucid.ava.blocks.colouring_table.GunColouringTableContainer;
import pellucid.ava.blocks.colouring_table.GunColouringTableTE;
import pellucid.ava.blocks.crafting_table.GunCraftingTableContainer;
import pellucid.ava.blocks.crafting_table.GunCraftingTableTE;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.items.init.*;
import pellucid.ava.misc.AVASounds;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class CommonModEventBus
{
    public static final ContainerType<GunCraftingTableContainer> GUN_CRAFTING_TABLE_CONTAINER = new ContainerType<>(GunCraftingTableContainer::new);
    public static final TileEntityType<GunCraftingTableTE> GUN_CRAFTING_TABLE_TE = TileEntityType.Builder.create(GunCraftingTableTE::new, AVABlocks.GUN_CRAFTING_TABLE).build(null);

    public static final ContainerType<GunColouringTableContainer> GUN_COLOURING_TABLE_CONTAINER = new ContainerType<>(GunColouringTableContainer::new);
    public static final TileEntityType<GunColouringTableTE> GUN_COLOURING_TABLE_TE = TileEntityType.Builder.create(GunColouringTableTE::new, AVABlocks.GUN_COLOURING_TABLE).build(null);

    public static final TileEntityType<BoostTileEntity> BOOST_TE = TileEntityType.Builder.create(BoostTileEntity::new, AVABlocks.ATTACK_DAMAGE_BOOST_BLOCK, AVABlocks.HEALTH_BOOST_BLOCK).build(null);

    @SubscribeEvent
    public static void onSetup(FMLCommonSetupEvent event)
    {
        DeferredWorkQueue.runLater(() ->
        {
            AVAEntities.addSpawns();
            ForgeRegistries.BIOMES.forEach((biome) ->
                    AVAEntities.ALL_HOSTILES.forEach((type) ->
                    {
                        AVAHostileEntity.SpawningConditionChances chances = AVAEntities.getRarityFor(type);
                        biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(type, chances.getWeight(), chances.getMin(), chances.getMax()));
                    }));
        });
    }

    @SubscribeEvent
    public static void onItemsRegister(RegistryEvent.Register<Item> event)
    {
        IForgeRegistry<Item> registry = event.getRegistry();
        Projectiles.registerAll(registry);
        Pistols.registerAll(registry);
        Rifles.registerAll(registry);
        Snipers.registerAll(registry);
        SubmachineGuns.registerAll(registry);
        MiscItems.registerAll(registry);
        SpecialWeapons.registerAll(registry);
        MeleeWeapons.registerAll(registry);
        AVABlocks.registerAllItems(registry);
    }

    @SubscribeEvent
    public static void onBlocksRegister(RegistryEvent.Register<Block> event)
    {
        AVABlocks.registerAllBlocks(event.getRegistry());
    }

    @SubscribeEvent
    public static void onEntitiesRegister(RegistryEvent.Register<EntityType<?>> event)
    {
        AVAEntities.registerAll(event.getRegistry());
    }

    @SubscribeEvent
    public static void registerSounds(RegistryEvent.Register<SoundEvent> event)
    {
        AVASounds.registerAll(event.getRegistry());
    }

    @SubscribeEvent
    public static void registerTE(RegistryEvent.Register<TileEntityType<?>> event)
    {
        GUN_CRAFTING_TABLE_TE.setRegistryName("gun_crafting_table_te");
        GUN_COLOURING_TABLE_TE.setRegistryName("gun_colouring_table_te");
        BOOST_TE.setRegistryName("boost_te");
        event.getRegistry().registerAll(
                GUN_CRAFTING_TABLE_TE,
                GUN_COLOURING_TABLE_TE,
                BOOST_TE
        );
    }

    @SubscribeEvent
    public static void registerContainer(RegistryEvent.Register<ContainerType<?>> event)
    {
        GUN_CRAFTING_TABLE_CONTAINER.setRegistryName("gun_crafting_table_container");
        GUN_COLOURING_TABLE_CONTAINER.setRegistryName("gun_colouring_table_container");
        event.getRegistry().registerAll(
                GUN_CRAFTING_TABLE_CONTAINER,
                GUN_COLOURING_TABLE_CONTAINER
        );
    }
}
