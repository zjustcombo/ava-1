package pellucid.ava.entities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

import javax.annotation.Nullable;
import java.util.UUID;

public abstract class ProjectileEntity extends AVAEntity implements IEntityAdditionalSpawnData
{
    protected int range;
    protected UUID shooterID;
    protected double initialVelocity;

    public ProjectileEntity(EntityType<? extends ProjectileEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public ProjectileEntity(EntityType<? extends ProjectileEntity> type, LivingEntity shooter, World worldIn, double velocity, int range)
    {
        this(type, worldIn);
        this.range = range;
        this.shooterID = shooter.getUniqueID();
        this.initialVelocity = velocity;
        float x = -MathHelper.sin(shooter.rotationYaw * ((float)Math.PI / 180F)) * MathHelper.cos(shooter.rotationPitch * ((float)Math.PI / 180F));
        float y = -MathHelper.sin((shooter.rotationPitch) * ((float)Math.PI / 180F));
        float z = MathHelper.cos(shooter.rotationYaw * ((float)Math.PI / 180F)) * MathHelper.cos(shooter.rotationPitch * ((float)Math.PI / 180F));
        Vec3d vec3d = (new Vec3d(x, y, z)).normalize().scale(velocity);
        this.setMotion(vec3d);
        this.setDirection();
        setPosition(shooter.getPosX(), shooter.getPosYEye() - (double)0.06F, shooter.getPosZ());
    }

    @Override
    public boolean isInRangeToRenderDist(double distance)
    {
        return distance <= 2048.0D;
    }

    @Override
    public void fromMob(LivingEntity target)
    {
        super.fromMob(target);
        double x = target.getPosX() - this.getPosX();
        double y = target.getPosYEye() - this.getPosYEye();
        double z = target.getPosZ() - this.getPosZ();
        Vec3d vec3d = (new Vec3d(x, y, z)).normalize().scale(initialVelocity);
        this.setMotion(vec3d);
        setDirection();
    }

    protected void setDirection()
    {
        Vec3d vec3d = getMotion();
        float f = MathHelper.sqrt(horizontalMag(vec3d));
        this.rotationYaw = (float)(MathHelper.atan2(vec3d.x, vec3d.z) * (double)(180F / (float)Math.PI));
        this.rotationPitch = (float)(MathHelper.atan2(vec3d.y, f) * (double)(180F / (float)Math.PI));
        this.prevRotationYaw = this.rotationYaw;
        this.prevRotationPitch = this.rotationPitch;
    }

    @Override
    public void writeSpawnData(PacketBuffer buffer)
    {
        buffer.writeInt(range);
    }

    @Override
    public void readSpawnData(PacketBuffer additionalData)
    {
        range = additionalData.readInt();
    }

    @Override
    public void tick()
    {
        super.tick();
        setDirection();
        AxisAlignedBB axisalignedbb = this.getBoundingBox().expand(this.getMotion());
        Vec3d from = this.getPositionVec();
        Vec3d to = this.getPositionVec().add(new Vec3d(this.getMotion().getX(), this.getMotion().getY(), this.getMotion().getZ()));
        EntityRayTraceResult entityResult = ProjectileHelper.rayTraceEntities(world, this, from, to, axisalignedbb, (entity) -> entity != getShooter() && !entity.isSpectator() && entity.canBeCollidedWith() && canAttack(entity));
        BlockRayTraceResult blockResult = world.rayTraceBlocks(new RayTraceContext(from, to, RayTraceContext.BlockMode.OUTLINE, RayTraceContext.FluidMode.NONE, this));
        if (blockResult.getType() != RayTraceResult.Type.MISS && entityResult != null)
            onImpact((from.distanceTo(blockResult.getHitVec()) > from.distanceTo(entityResult.getHitVec())) ? entityResult : blockResult);
        else if (entityResult != null)
            onImpact(entityResult);
        else if (blockResult.getType() != RayTraceResult.Type.MISS)
            onImpact(blockResult);
        move();
    }

    protected void move()
    {
        move(false);
    }

    protected void move(boolean rotates)
    {
        Vec3d vec3d = this.getMotion();
        double x = this.getPosX() + vec3d.x;
        double y = this.getPosY() + vec3d.y;
        double z = this.getPosZ() + vec3d.z;
        if (this.isInWater())
            for(int i = 0; i < 5; i++)
                this.world.addParticle(ParticleTypes.BUBBLE, x - vec3d.x * 0.25D, y - vec3d.y * 0.25D, z - vec3d.z * 0.25D, vec3d.x, vec3d.y, vec3d.z);
        this.setMotion(vec3d.x, vec3d.y - getGravityVelocity(), vec3d.z);
        this.setPosition(x, y, z);
        if (rotates)
        {
            this.rotationPitch += 6.0F;
            this.rotationYaw += 6.0F;
        }
    }

    protected double getGravityVelocity()
    {
        return 0.055D;
    }

    protected void onImpact(RayTraceResult result)
    {

    }

    @Nullable
    public LivingEntity getShooter()
    {
        if (this.shooterID != null && this.world instanceof ServerWorld)
        {
            Entity entity = ((ServerWorld)this.world).getEntityByUuid(shooterID);
            return entity == null ? null : (LivingEntity)entity;
        }
        return null;
    }

    @Override
    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putInt("range", this.range);
        compound.putDouble("velocityi", this.initialVelocity);
        if (this.shooterID != null)
            compound.putUniqueId("owner", this.shooterID);
    }

    @Override
    public void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        this.range = compound.getInt("range");
        this.initialVelocity = compound.getDouble("velocityi");
        if (compound.contains("owner"))
            this.shooterID = compound.getUniqueId("owner");
    }
}
