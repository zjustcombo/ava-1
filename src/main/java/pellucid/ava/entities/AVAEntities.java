package pellucid.ava.entities;

import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.entities.kits.AmmoKitEntity;
import pellucid.ava.entities.kits.FirstAidKitEntity;
import pellucid.ava.entities.livings.*;
import pellucid.ava.entities.scanhits.BinocularRTBulletEntity;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.entities.scanhits.MeleeAttackRaytraceEntity;
import pellucid.ava.entities.shootables.M202RocketEntity;
import pellucid.ava.entities.throwables.GrenadeEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.entities.throwables.ToxicSmokeGrenadeEntity;

import java.util.HashSet;

import static pellucid.ava.entities.livings.AVAHostileEntity.SpawningConditionChances.*;

public class AVAEntities
{
    public static final EntityType<?>
            BULLET = EntityType.Builder.<BulletEntity>create(BulletEntity::new, EntityClassification.MISC)
            .size(0.1F, 0.1F)
            .build("bullet")
            .setRegistryName("bullet"),

            BINOCULAR_BULLET = EntityType.Builder.<BinocularRTBulletEntity>create(BinocularRTBulletEntity::new, EntityClassification.MISC)
            .size(0.1F, 0.1F)
            .build("binocular_bullet")
            .setRegistryName("binocular_bullet"),

            MELEE_RAYTRACING = EntityType.Builder.<MeleeAttackRaytraceEntity>create(MeleeAttackRaytraceEntity::new, EntityClassification.MISC)
            .size(0.1F, 0.1F)
            .build("melee_raytracing")
            .setRegistryName("melee_raytracing"),

            M67 = EntityType.Builder.<HandGrenadeEntity>create(HandGrenadeEntity::new, EntityClassification.MISC)
            .size(0.3F, 0.3F)
            .build("m67")
            .setRegistryName("m67"),

            MK3A2 = EntityType.Builder.<HandGrenadeEntity>create(HandGrenadeEntity::new, EntityClassification.MISC)
            .size(0.3F, 0.3F)
            .build("mk3a2")
            .setRegistryName("mk3a2"),

            M116A1 = EntityType.Builder.<HandGrenadeEntity>create(HandGrenadeEntity::new, EntityClassification.MISC)
            .size(0.3F, 0.3F)
            .build("m116a1")
            .setRegistryName("m116a1"),

            M18 = EntityType.Builder.<SmokeGrenadeEntity>create(SmokeGrenadeEntity::new, EntityClassification.MISC)
            .size(0.3F, 0.3F)
            .build("m18")
            .setRegistryName("m18"),

            M18_TOXIC = EntityType.Builder.<ToxicSmokeGrenadeEntity>create(ToxicSmokeGrenadeEntity::new, EntityClassification.MISC)
            .size(0.3F, 0.3F)
            .build("m18_toxic")
            .setRegistryName("m18_toxic"),

            AMMO_KIT = EntityType.Builder.<AmmoKitEntity>create(AmmoKitEntity::new, EntityClassification.MISC)
            .size(0.5F, 0.5F)
            .build("ammo_kit")
            .setRegistryName("ammo_kit"),

            FIRST_AID_KIT = EntityType.Builder.<FirstAidKitEntity>create(FirstAidKitEntity::new, EntityClassification.MISC)
            .size(0.5F, 0.5F)
            .build("first_aid_kit")
            .setRegistryName("first_aid_kit"),

            ROCKET = EntityType.Builder.<M202RocketEntity>create(M202RocketEntity::new, EntityClassification.MISC)
                    .size(0.3F, 0.3F)
                    .build("rocket")
                    .setRegistryName("rocket"),

            GRENADE = EntityType.Builder.<GrenadeEntity>create(GrenadeEntity::new, EntityClassification.MISC)
                    .size(0.3F, 0.3F)
                    .build("grenade")
                    .setRegistryName("grenade");

    public static final EntityType<MeleeGuardEntity>
            MELEE_GUARD = (EntityType<MeleeGuardEntity>) EntityType.Builder.<MeleeGuardEntity>create(MeleeGuardEntity::new, EntityClassification.MONSTER)
                    .size(0.6F, 1.8F)
                    .build("melee_guard")
                    .setRegistryName("melee_guard");

    public static final EntityType<RifleGuardEntity>
            RIFLE_GUARD = (EntityType<RifleGuardEntity>) EntityType.Builder.<RifleGuardEntity>create(RifleGuardEntity::new, EntityClassification.MONSTER)
                    .size(0.6F, 1.8F)
                    .build("rifle_guard")
                    .setRegistryName("rifle_guard");

    public static final EntityType<GrenadeLauncherGuardEntity>
            GRENADE_LAUNCHER_GUARD = (EntityType<GrenadeLauncherGuardEntity>) EntityType.Builder.<GrenadeLauncherGuardEntity>create(GrenadeLauncherGuardEntity::new, EntityClassification.MONSTER)
            .size(0.6F, 1.8F)
            .build("grenade_launcher_guard")
            .setRegistryName("grenade_launcher_guard");

    public static final EntityType<PistolGuardEntity>
            PISTOL_GUARD = (EntityType<PistolGuardEntity>) EntityType.Builder.<PistolGuardEntity>create(PistolGuardEntity::new, EntityClassification.MONSTER)
            .size(0.6F, 1.8F)
            .build("pistol_guard")
            .setRegistryName("pistol_guard");

    public static final EntityType<ToxicSmokeGuardEntity>
            TOXIC_SMOKE_GUARD = (EntityType<ToxicSmokeGuardEntity>) EntityType.Builder.<ToxicSmokeGuardEntity>create(ToxicSmokeGuardEntity::new, EntityClassification.MONSTER)
            .size(0.6F, 1.8F)
            .build("toxic_smoke_guard")
            .setRegistryName("toxic_smoke_guard");


    public static void registerAll(IForgeRegistry<EntityType<?>> registry)
    {
        registry.registerAll(
                BULLET,
                BINOCULAR_BULLET,
                MELEE_RAYTRACING,
                M67,
                MK3A2,
                M116A1,
                M18,
                M18_TOXIC,
                AMMO_KIT,
                FIRST_AID_KIT,
                ROCKET,
                GRENADE,

                MELEE_GUARD,
                RIFLE_GUARD,
                GRENADE_LAUNCHER_GUARD,
                PISTOL_GUARD,
                TOXIC_SMOKE_GUARD
        );
    }

    public static void addSpawns()
    {
        registerSpawnPlacement(MELEE_GUARD, COMMON);
        registerSpawnPlacement(RIFLE_GUARD, UNCOMMON);
        registerSpawnPlacement(GRENADE_LAUNCHER_GUARD, RARE);
        registerSpawnPlacement(PISTOL_GUARD, NORMAL);
        registerSpawnPlacement(TOXIC_SMOKE_GUARD, RARE);
    }

    public static HashSet<EntityType<?>> ALL_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> UNCOMMON_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> COMMON_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> NORMAL_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> RARE_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> EXTREMELY_RARE_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> BOSS_HOSTILES = new HashSet<>();

    private static void registerSpawnPlacement(EntityType<? extends AVAHostileEntity> type, AVAHostileEntity.SpawningConditionChances chance)
    {
        switch (chance)
        {
            case UNCOMMON:
                UNCOMMON_HOSTILES.add(type);
                break;
            case COMMON:
                COMMON_HOSTILES.add(type);
                break;
            case NORMAL:
                NORMAL_HOSTILES.add(type);
                break;
            case RARE:
                RARE_HOSTILES.add(type);
                break;
            case EXTREMELY_RARE:
                EXTREMELY_RARE_HOSTILES.add(type);
                break;
            case BOSS:
                BOSS_HOSTILES.add(type);
                break;
        }
        ALL_HOSTILES.add(type);
        EntitySpawnPlacementRegistry.register(type, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING, (type2, world, reason, pos, rand) -> chance.test((EntityType<AVAHostileEntity>) type2, world, reason, pos, rand));
    }

    public static AVAHostileEntity.SpawningConditionChances getRarityFor(EntityType<?> type)
    {
        if (COMMON_HOSTILES.contains(type))
            return COMMON;
        else if (UNCOMMON_HOSTILES.contains(type))
            return UNCOMMON;
        else if (NORMAL_HOSTILES.contains(type))
            return NORMAL;
        else if (RARE_HOSTILES.contains(type))
            return RARE;
        else if (EXTREMELY_RARE_HOSTILES.contains(type))
            return EXTREMELY_RARE;
//        else if (BOSS_HOSTILES.contains(type))
        else
            return BOSS;
    }
}
