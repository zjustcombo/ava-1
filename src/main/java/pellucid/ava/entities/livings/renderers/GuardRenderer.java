package pellucid.ava.entities.livings.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import pellucid.ava.AVA;
import pellucid.ava.entities.livings.AVAHostileEntity;

public class GuardRenderer<E extends AVAHostileEntity> extends BipedRenderer<E, GuardModel<E>>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/guard_blue.png");

    public GuardRenderer(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn, new GuardModel<>(0.7F), 0.5F);
    }

    @Override
    public void render(E entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
    {
        matrixStackIn.push();
        matrixStackIn.scale(0.9F, 0.9F, 0.9F);
        super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
        matrixStackIn.pop();
    }

    @Override
    public ResourceLocation getEntityTexture(E entity)
    {
        return entity.getColour().texture;
    }
}
