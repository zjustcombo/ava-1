package pellucid.ava.entities.livings.renderers;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.util.HandSide;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.items.guns.AVAItemGun;

public class GuardModel<E extends AVAHostileEntity> extends BipedModel<E>
{
    public GuardModel(float modelSize)
    {
        super(modelSize);
    }

    @Override
    public void setLivingAnimations(E entityIn, float limbSwing, float limbSwingAmount, float partialTick)
    {
        super.setLivingAnimations(entityIn, limbSwing, limbSwingAmount, partialTick);
        if (entityIn.getHeldItemMainhand().getItem() instanceof AVAItemGun)
        {
            if (entityIn.getPrimaryHand() == HandSide.RIGHT)
                this.rightArmPose = BipedModel.ArmPose.BOW_AND_ARROW;
            else
                this.leftArmPose = BipedModel.ArmPose.BOW_AND_ARROW;
        }
    }
}
