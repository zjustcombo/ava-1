package pellucid.ava.entities.livings;

import com.google.common.collect.ImmutableMap;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.entity.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;
import pellucid.ava.AVA;
import pellucid.ava.misc.config.AVAServerConfig;

import java.util.Random;

public abstract class AVAHostileEntity extends CreatureEntity implements IEntityAdditionalSpawnData
{
    private static final DataParameter<String> COLOUR = EntityDataManager.createKey(AVAHostileEntity.class, DataSerializers.STRING);

    public AVAHostileEntity(EntityType<? extends CreatureEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public ILivingEntityData onInitialSpawn(IWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, ILivingEntityData spawnDataIn, CompoundNBT dataTag)
    {
        setEquipmentBasedOnDifficulty(difficultyIn);
        return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
    }

    @Override
    protected void setEquipmentBasedOnDifficulty(DifficultyInstance difficulty)
    {
        setItemStackToSlot(EquipmentSlotType.MAINHAND, getWeapon());
    }

    @Override
    protected void registerData()
    {
        super.registerData();
        getDataManager().register(COLOUR, getDefaultColour().toString());
    }

    protected ColourTypes getDefaultColour()
    {
        return ColourTypes.randomColourNotEmpty();
    }

    protected ItemStack getWeapon()
    {
        return new ItemStack(Items.AIR);
    }

    @Override
    public boolean canBeLeashedTo(PlayerEntity player)
    {
        return false;
    }

    @Override
    public boolean onLivingFall(float distance, float damageMultiplier)
    {
        return false;
    }

    @Override
    protected int decreaseAirSupply(int air)
    {
        return air;
    }

    @Override
    public boolean canDespawn(double distanceToClosestPlayer)
    {
        return false;
    }

    @Override
    public void writeSpawnData(PacketBuffer buffer)
    {
        buffer.writeString(getColour().toString());
    }

    @Override
    public void readSpawnData(PacketBuffer additionalData)
    {
        setColour(ColourTypes.valueOf(additionalData.readString(32767)));
    }

    @OnlyIn(Dist.CLIENT)
    public void modifyItemModel(Vector3f rotation, Vector3f translation, Vector3f scale)
    {

    }

    public void setColour(ColourTypes colour)
    {
        getDataManager().set(COLOUR, colour.toString());
    }

    public ColourTypes getColour()
    {
        return ColourTypes.valueOf(getDataManager().get(COLOUR));
    }

    @Override
    public IPacket<?> createSpawnPacket()
    {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected void dropLoot(DamageSource damageSourceIn, boolean attackedRecently)
    {
        super.dropLoot(damageSourceIn, attackedRecently);
        getConstantDrops().forEach((item, count) -> {
            ItemStack stack = new ItemStack(item);
            stack.setCount(count);
            stack.grow(rand.nextInt(3) - 1);
            if (!stack.isEmpty())
                entityDropItem(stack);
        });
    }

    protected ImmutableMap<Item, Integer> getConstantDrops()
    {
        return ImmutableMap.of();
    }

    @Override
    protected void dropSpecialItems(DamageSource source, int looting, boolean recentlyHitIn)
    {
        if (rand.nextInt(10) == 0)
        {
            ItemStack stack = getHeldItemMainhand();
            stack.setDamage(stack.getMaxDamage());
            entityDropItem(stack);
            setItemStackToSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY);
        }
    }

    public enum ColourTypes
    {
        NONE("none"),
        BLUE("blue"),
        YELLOW("yellow"),
        RED("red");

        public ResourceLocation texture;
        private static final Random RANDOM = new Random();

        ColourTypes(String colour)
        {
            this.texture = new ResourceLocation(AVA.MODID, "textures/entities/guard_" + colour + ".png");
        }

        public static ColourTypes randomColourNotEmpty()
        {
            return values()[RANDOM.nextInt(values().length - 1) + 1];
        }

        public static ColourTypes randomColourFrom(ColourTypes... types)
        {
            return types[RANDOM.nextInt(types.length)];
        }
    }

    public enum SpawningConditionChances implements EntitySpawnPlacementRegistry.IPlacementPredicate<AVAHostileEntity>
    {
        COMMON(50, 1, 5),
        UNCOMMON(20, 1, 4),
        NORMAL(10, 1, 3),
        RARE(5, 0, 2),
        EXTREMELY_RARE(3, 0, 1),
        BOSS(1, 0, 1);

        private final int weight;
        private final int min;
        private final int max;

        SpawningConditionChances(int weight, int min, int max)
        {
            this.weight = weight;
            this.min = min;
            this.max = max;
        }

        public int getWeight()
        {
            return weight;
        }

        public int getMin()
        {
            return min;
        }

        public int getMax()
        {
            return max;
        }

        @Override
        public boolean test(EntityType<AVAHostileEntity> p_test_1_, IWorld p_test_2_, SpawnReason p_test_3_, BlockPos p_test_4_, Random rand)
        {
            return AVAServerConfig.SHOULD_GUARDS_SPAWN.get() && rand.nextInt(this == BOSS ? 100 : 10) == 0 && rand.nextInt(101) > 100 - AVAServerConfig.GUARDS_SPAWN_CHANCE.get();
        }
    }
}
