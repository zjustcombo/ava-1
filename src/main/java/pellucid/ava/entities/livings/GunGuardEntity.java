package pellucid.ava.entities.livings;

import com.google.common.collect.ImmutableMap;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.items.guns.AVAItemGun;

public abstract class GunGuardEntity extends RangedGuardEntity
{
    public GunGuardEntity(EntityType<? extends CreatureEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public float getSpread(float spread)
    {
        return 2.75F;
    }

    @Override
    public void livingTick()
    {
        super.livingTick();
        ItemStack stack = getHeldItemMainhand();
        if (stack.getItem() instanceof AVAItemGun)
        {
            AVAItemGun gun = (AVAItemGun) stack.getItem();
            CompoundNBT compound = gun.initTags(stack);
            if (compound.getInt("ammo") != stack.getMaxDamage() - stack.getDamage())
                stack.setDamage(gun.getMaxAmmo() - compound.getInt("ammo"));
            if (compound.getInt("ticks") > 0)
                compound.putInt("ticks", compound.getInt("ticks") - 1);
            if (compound.getInt("fire") > 0)
            {
                compound.putInt("fire", compound.getInt("fire") + 1);
                if (compound.getInt("fire") >= gun.getFireAnimation())
                    compound.putInt("fire", 0);
            }
            if (compound.getInt("reload") > 0)
            {
                compound.putInt("reload", compound.getInt("reload") + 1);
                if (compound.getInt("reload") >= gun.getReloadTime())
                    gun.reload(compound, this, stack);
            }
        }
    }

    @Override
    public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor)
    {
        ItemStack stack = getHeldItemMainhand();
        if (stack.getItem() instanceof AVAItemGun)
        {
            AVAItemGun gun = (AVAItemGun) stack.getItem();
            if (gun.firable(this, stack))
                gun.fire(world, this, target, stack);
            CompoundNBT compound = gun.initTags(stack);
            if (compound.getInt("ammo") < 1 && gun.reloadable(this, stack))
                gun.preReload(this, stack);
        }
    }

    private final ImmutableMap<Item, Integer> DROPS = ImmutableMap.of(((AVAItemGun) getWeapon().getItem()).getMagazineType(), 2);
    @Override
    protected ImmutableMap<Item, Integer> getConstantDrops()
    {
        return DROPS;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void modifyItemModel(Vector3f rotation, Vector3f translation, Vector3f scale)
    {
        rotation.mul(0.0F, 1.0F, 1.0F);
    }
}
