package pellucid.ava.entities.livings;

import net.minecraft.client.renderer.Vector3f;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.Rifles;

public class RifleGuardEntity extends GunGuardEntity
{
    public RifleGuardEntity(EntityType<? extends RifleGuardEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected ItemStack getWeapon()
    {
        ItemStack weapon = new ItemStack(Rifles.M4A1);
        AVAItemGun gun = (AVAItemGun) weapon.getItem();
        gun.reload(gun.initTags(weapon), this, weapon);
        return weapon;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void modifyItemModel(Vector3f rotation, Vector3f translation, Vector3f scale)
    {
        super.modifyItemModel(rotation, translation, scale);
        translation.set(0.0F, -1.25F, 0.0F);
    }
}
