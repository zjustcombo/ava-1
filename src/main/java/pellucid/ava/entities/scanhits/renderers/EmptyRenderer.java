package pellucid.ava.entities.scanhits.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.AVA;

@OnlyIn(Dist.CLIENT)
public class EmptyRenderer<T extends Entity> extends EntityRenderer<T>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/bullet.png");
//    private final BulletModel bulletModel = new BulletModel();

    public EmptyRenderer(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn);
    }

    @Override
    public void render(T entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
    {
//        float f = MathHelper.rotLerp(entityIn.prevRotationYaw, entityIn.rotationYaw, partialTicks);
//        float f1 = MathHelper.lerp(partialTicks, entityIn.prevRotationPitch, entityIn.rotationPitch);
//        this.bulletModel.func_225603_a_(0.0F, f, f1);
//        bulletModel.render(matrixStackIn, bufferIn.getBuffer(this.bulletModel.getRenderType(this.getEntityTexture(entityIn))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
    }

    @Override
    public ResourceLocation getEntityTexture(T entity)
    {
        return TEXTURE;
    }
}
