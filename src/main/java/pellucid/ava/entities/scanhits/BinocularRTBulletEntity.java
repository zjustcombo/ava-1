package pellucid.ava.entities.scanhits;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.items.miscs.Binocular;
import pellucid.ava.misc.cap.PlayerAction;

import javax.annotation.Nullable;

public class BinocularRTBulletEntity extends HitScanEntity
{
    private boolean forRange;
    public BinocularRTBulletEntity(EntityType<?> type, World worldIn)
    {
        super(type, worldIn);
    }

    public BinocularRTBulletEntity(World worldIn, PlayerEntity shooter, boolean forRange)
    {
        super(AVAEntities.BULLET, worldIn, shooter, 200);
        this.forRange = forRange;
    }

    @Override
    protected void onImpact(@Nullable RayTraceResult result)
    {
        LivingEntity player = getShooter();
        if (forRange)
        {
            if (player != null)
            {
                if (player.getHeldItemMainhand().getItem() instanceof Binocular)
                {
                    int distance;
                    if (result == null)
                        distance = -1;
                    else
                        distance = (int) getPositionVec().distanceTo(result.getHitVec());
                    player.getHeldItemMainhand().getOrCreateTag().putInt("hit", distance);
                }
            }
        }
        else if (result != null && result.getType() == RayTraceResult.Type.ENTITY)
        {
            Entity target = ((EntityRayTraceResult) result).getEntity();
            if (target instanceof ServerPlayerEntity && !((PlayerEntity) target).isCreative() && !target.isSpectator() && getShooter() != null && !getShooter().isOnSameTeam(target))
            {
                PlayerAction.getCap((PlayerEntity) target).setUAVDuration((PlayerEntity) target, 140);
                if (player != null)
                    world.getPlayers().forEach((player2) -> {
                        if (player.isOnSameTeam(player2))
                            player2.sendMessage(new StringTextComponent(player.getDisplayName() + " has located enemy " + target.getDisplayName() + " at " + target.getPosition().toString()));
                    });
            }
        }
        this.remove();
    }

    @Override
    protected void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putBoolean("forrange", forRange);
    }

    @Override
    protected void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        forRange = compound.getBoolean("forrange");
    }
}
