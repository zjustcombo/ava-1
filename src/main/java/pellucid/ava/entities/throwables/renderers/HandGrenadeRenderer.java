package pellucid.ava.entities.throwables.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.AVA;
import pellucid.ava.entities.throwables.HandGrenadeEntity;

@OnlyIn(Dist.CLIENT)
public class HandGrenadeRenderer extends EntityRenderer<HandGrenadeEntity>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/m67.png");
    private final M67Model m67Model = new M67Model();

    public HandGrenadeRenderer(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn);
    }

    @Override
    public void render(HandGrenadeEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
    {
        float f = MathHelper.rotLerp(entityIn.prevRotationYaw, entityIn.rotationYaw, partialTicks);
        float f1 = MathHelper.lerp(partialTicks, entityIn.prevRotationPitch, entityIn.rotationPitch);
        this.m67Model.func_225603_a_(0.0F, f, f1);
        m67Model.render(matrixStackIn, bufferIn.getBuffer(this.m67Model.getRenderType(this.getEntityTexture(entityIn))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
    }

    @Override
    public ResourceLocation getEntityTexture(HandGrenadeEntity entity)
    {
        return TEXTURE;
    }
}
