package pellucid.ava.entities.throwables.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

import java.util.function.Consumer;

public class ProjectileRenderer<E extends Entity> extends EntityRenderer<E>
{
    protected final Model model;
    private final ResourceLocation texturePath;
    private final Consumer<MatrixStack> modification;

    public ProjectileRenderer(EntityRendererManager manager, Model model, ResourceLocation texturePath)
    {
        this(manager, model, texturePath, (stack) -> {});
    }

    public ProjectileRenderer(EntityRendererManager manager, Model model, ResourceLocation texturePath, Consumer<MatrixStack> modification)
    {
        super(manager);
        this.model = model;
        this.texturePath = texturePath;
        this.modification = modification;
    }

    @Override
    public void render(E entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
    {
        matrixStackIn.push();
        matrixStackIn.rotate(Vector3f.YP.rotationDegrees(MathHelper.lerp(partialTicks, entityIn.prevRotationYaw, entityIn.rotationYaw) - 180.0F));
        matrixStackIn.rotate(Vector3f.XP.rotationDegrees(MathHelper.lerp(partialTicks, entityIn.prevRotationPitch, entityIn.rotationPitch)));
        modification.accept(matrixStackIn);
        model.render(matrixStackIn, bufferIn.getBuffer(model.getRenderType(getEntityTexture(entityIn))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
        matrixStackIn.pop();
    }

    @Override
    public ResourceLocation getEntityTexture(E entity)
    {
        return texturePath;
    }
}
