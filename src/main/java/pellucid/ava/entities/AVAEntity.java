package pellucid.ava.entities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.fml.network.PacketDistributor;
import pellucid.ava.misc.packets.AVAPackets;
import pellucid.ava.misc.packets.PlaySoundToClientMessage;

public abstract class AVAEntity extends Entity
{
    protected int rangeTravelled;
    public boolean fromMob;

    public AVAEntity(EntityType<?> entityTypeIn, World worldIn)
    {
        super(entityTypeIn, worldIn);
    }

    @Override
    public void tick()
    {
        super.tick();
        rangeTravelled++;

    }

    protected void playSound(SoundEvent sound, SoundCategory category, double x, double y, double z, float volume, float pitch)
    {
        AVAPackets.INSTANCE.send(PacketDistributor.ALL.noArg(), new PlaySoundToClientMessage(sound.getRegistryName().toString(), category, x, y, z, volume, pitch));
    }

    @Override
    public void playSound(SoundEvent soundIn, float volume, float pitch)
    {
        if (world instanceof ServerWorld)
            playSound(soundIn, SoundCategory.PLAYERS, getPosX(), getPosY(), getPosZ(), 1.0F, 1.0F);
    }

    public void fromMob(LivingEntity target)
    {
        this.fromMob = true;
    }

    protected boolean canAttack(Entity entity)
    {
        return !fromMob || entity instanceof PlayerEntity;
    }

    @Override
    protected void registerData()
    {

    }

    @Override
    protected void writeAdditional(CompoundNBT compound)
    {
        compound.putInt("rangetravelled", rangeTravelled);
        compound.putBoolean("frommob", fromMob);
    }

    @Override
    protected void readAdditional(CompoundNBT compound)
    {
        rangeTravelled = compound.getInt("rangetravelled");
        fromMob = compound.getBoolean("frommob");
    }

    @Override
    public IPacket<?> createSpawnPacket()
    {
        return NetworkHooks.getEntitySpawningPacket(this);
    }
}
