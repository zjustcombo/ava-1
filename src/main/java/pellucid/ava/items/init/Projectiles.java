package pellucid.ava.items.init;

import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.items.throwables.HandGrenadeItem;
import pellucid.ava.items.throwables.SmokeGrenade;
import pellucid.ava.items.throwables.ToxicSmokeGrenade;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;

import java.util.ArrayList;

public class Projectiles
{
    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * assault rifle (e.g. aks-74u): +5.5
     * pistols (e.g. p226): +6
     */
    public static Item M67 = null;
    public static Item MK3A2 = null;
    public static Item M116A1 = null;

    public static Item M18_GREY = null;
    public static Item M18_PURPLE = null;
    public static Item M18_TOXIC = null;

    public static ArrayList<Item> ITEM_PROJECTILES = new ArrayList<>();

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        registry.registerAll(
                M67 = new HandGrenadeItem(new Item.Properties(), false, 0, 130, 1.0F, 60, 3.0F, (EntityType<HandGrenadeEntity>) AVAEntities.M67,
                AVASounds.GENERIC_GRENADE_EXPLODE, new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_NUGGET, 3).addItem(Items.CLAY_BALL).addItem(Items.SAND), AVAWeaponUtil.Classification.PROJECTILE).setRegistryName("m67"),
                MK3A2 = new HandGrenadeItem(new Item.Properties(), false, 60, 130, 0.75F, 30, 2.0F, (EntityType<HandGrenadeEntity>) AVAEntities.MK3A2,
                AVASounds.FLASH_GRENADE_EXPLODE, new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_NUGGET, 3).addItem(Items.CLAY_BALL).addItem(Items.GLOWSTONE_DUST).addDescription("mk3a2_1").addDescription("mk3a2_2"), AVAWeaponUtil.Classification.PROJECTILE).setRegistryName("mk3a2"),
                M116A1 = new HandGrenadeItem(new Item.Properties(), false, 80, 0, 0.75F, 30, 5.0F, (EntityType<HandGrenadeEntity>) AVAEntities.M116A1,
                AVASounds.FLASH_GRENADE_EXPLODE, new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_NUGGET, 3).addItem(Items.GLOWSTONE_DUST).addDescription("m116a1_1"), AVAWeaponUtil.Classification.PROJECTILE).setRegistryName("m116a1"),

                M18_GREY = new SmokeGrenade(new Recipe().addItem(Items.LIGHT_GRAY_DYE, 2), new int[] {255, 255, 255}, "grey").setRegistryName("m18_grey"),
                M18_PURPLE = new SmokeGrenade(new Recipe().addItem(Items.PURPLE_DYE, 2), new int[] {100, 0, 230}, "purple").setRegistryName("m18_purple"),
                M18_TOXIC = new ToxicSmokeGrenade(new Recipe().addItem(Items.SPIDER_EYE, 6), new int[] {73, 217, 63}, "lime").setRegistryName("m18_toxic")
        );
    }
}
