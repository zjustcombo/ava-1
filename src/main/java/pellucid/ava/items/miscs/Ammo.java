package pellucid.ava.items.miscs;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import pellucid.ava.misc.AVAItemGroups;

public class Ammo extends Item implements IHasRecipe
{
    protected final Recipe recipe;
    public final boolean isMagazine;

    public Ammo(Recipe recipe)
    {
        this(new Item.Properties(), recipe, false);
    }

    public Ammo(Properties properties, Recipe recipe, boolean isMagazine)
    {
        super(properties.group(AVAItemGroups.MAIN));
        this.recipe = recipe;
        this.isMagazine = isMagazine;
    }

    public void addToInventory(PlayerEntity player, int times)
    {
        player.inventory.addItemStackToInventory(new ItemStack(this, (this.isMagazine ? 1 : 5) * times));
    }

    @Override
    public Recipe getRecipe()
    {
        return this.recipe;
    }

    public boolean isMagazine()
    {
        return this.isMagazine;
    }

    @Override
    public int getItemStackLimit(ItemStack stack)
    {
        return (!this.isMagazine || this.getDamage(stack) == 0) ? 64 : 1;
    }
}
