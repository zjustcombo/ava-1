package pellucid.ava.items.miscs;

import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.misc.config.AVAServerConfig;

import java.util.ArrayList;
import java.util.HashMap;

public class Recipe
{
    private final HashMap<Item, Integer> itemCount = new HashMap<>();
    private final ArrayList<Item> ingredients = new ArrayList<>();
    private final ArrayList<String> description = new ArrayList<>();
    private int resultCount = 1;

    public Recipe()
    {
    }

    public Recipe addItem(Item item)
    {
        this.addItem(item, 1);
        return this;
    }

    public Recipe addItem(Item item, int count)
    {
        int limit = item.getItemStackLimit(new ItemStack(item));
        count = Math.max(Math.min(count, limit), 0);
        if (!this.ingredients.contains(item))
            this.ingredients.add(item);
        if (!this.itemCount.containsKey(item))
            this.itemCount.put(item, count);
        else
            this.itemCount.replace(item, Math.max(Math.min(this.itemCount.get(item) + count, limit), 0));
        return this;
    }

    public Recipe mergeIngredients(Recipe recipe)
    {
        for (Item ingredient : recipe.getIngredients())
            this.addItem(ingredient, recipe.getCount(ingredient));
        return this;
    }

    public boolean canCraft(PlayerEntity player, Item result)
    {
        if (player.isCreative())
            return true;
        if (!player.world.isRemote() && !isItemCraftable(result))
            return false;
        PlayerInventory inventory = player.inventory;
        for (Item ingredient : this.ingredients)
            if (!(getItemCount(inventory, ingredient) >= getCount(ingredient)))
                return false;
        return true;
    }

    private boolean isItemCraftable(Item item)
    {
        return item != AVABlocks.AMMO_KIT_SUPPLIER.asItem() || AVAServerConfig.IS_AMMO_KIT_SUPPLIER_CRAFTABLE.get();
    }

    public void craft(ServerPlayerEntity player, Item result)
    {
        PlayerInventory inventory = player.inventory;
        if (!player.isCreative())
            for (Item ingredient : this.ingredients)
            {
                int count = getCount(ingredient);
                while (count > 0)
                    for (int slot=0;slot<inventory.getSizeInventory();slot++)
                    {
                        ItemStack stack = inventory.getStackInSlot(slot);
                        if (stack.getItem() == ingredient)
                        {
                            if (stack.getCount() >= count)
                            {
                                stack.shrink(count);
                                count = 0;
                                break;
                            }
                            else
                            {
                                count -= stack.getCount();
                                inventory.removeStackFromSlot(slot);
                            }
                        }
                    }
            }
        ItemStack stack = new ItemStack(result);
        int count = this.getResultCount();
        stack.setCount(count);
        if (stack.getItem() instanceof AVAItemGun)
            stack.setDamage(stack.getMaxDamage());
        if (!inventory.addItemStackToInventory(stack))
            player.getEntityWorld().addEntity(new ItemEntity(player.getEntityWorld(), player.getPosX(), player.getPosY(), player.getPosZ(), stack));
    }

    protected int getItemCount(PlayerInventory inventory, Item item)
    {
        int count = 0;
        for (int slot=0;slot<inventory.getSizeInventory();slot++)
        {
            ItemStack stack = inventory.getStackInSlot(slot);
            if (stack.getItem() == item)
                count += stack.getCount();
        }
        return count;
    }

    public ArrayList<Item> getIngredients()
    {
        return this.ingredients;
    }

    public int getCount(Item item)
    {
        return this.itemCount.getOrDefault(item, 0);
    }

    public Recipe setResultCount(int count)
    {
        this.resultCount = count;
        return this;
    }

    public int getResultCount()
    {
        return this.resultCount;
    }

    public Recipe addDescription(String text)
    {
        this.description.add("ava.gui.description." + text);
        return this;
    }

    public boolean hasDescription()
    {
        return !this.description.isEmpty();
    }

    public ArrayList<String> getDescription()
    {
        return this.description;
    }

    public Recipe copy()
    {
        return copyOf(this);
    }

    public static Recipe copyOf(Recipe recipe)
    {
        Recipe newRecipe = new Recipe();
        for (Item ingredient : recipe.getIngredients())
            newRecipe.addItem(ingredient, recipe.getCount(ingredient));
        if (recipe.hasDescription())
            for (String description : recipe.getDescription())
                newRecipe.addDescription(description);
        newRecipe.setResultCount(recipe.getResultCount());
        return newRecipe;
    }
}
