package pellucid.ava.items.miscs;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.LazyValue;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.AVA;
import pellucid.ava.events.AVACommonEvent;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

public class Armours extends ArmorItem implements IHasRecipe
{
    protected static final AttributeModifier MOVEMENT_SPEED_MODIFIER = new AttributeModifier(UUID.fromString("D9AA8197-0727-4A85-80C4-E8823A26A765"), "Armour Speed Modifier", 0.07F, AttributeModifier.Operation.ADDITION);
    protected final Recipe recipe;
    public static final List<Armours> EU_ARMOURS = new ArrayList<>();
    public static final List<Armours> NRF_ARMOURS = new ArrayList<>();

    public Armours(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder, Recipe recipe)
    {
        super(materialIn, slot, builder);
        if (materialIn == AVAArmourMaterial.EU_STANDARD || materialIn == AVAArmourMaterial.NRF_STANDARD)
            for (int i=1;i<4;i++)
                recipe.addDescription("standard_armour_" + i);
        if (materialIn == AVAArmourMaterial.EU_STANDARD)
            EU_ARMOURS.add(this);
        else
            NRF_ARMOURS.add(this);
        this.recipe = recipe;
    }

    @Override
    public void onArmorTick(ItemStack stack, World world, PlayerEntity player)
    {
        IAttributeInstance speed = player.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED);
        if (AVACommonEvent.isFullEquipped(player) && player.isSneaking() && player.onGround)
        {
            if (!speed.hasModifier(MOVEMENT_SPEED_MODIFIER))
                speed.applyModifier(MOVEMENT_SPEED_MODIFIER);
        }
        else if (speed.hasModifier(MOVEMENT_SPEED_MODIFIER))
            speed.removeModifier(MOVEMENT_SPEED_MODIFIER);
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World world, List<ITextComponent> tooltip, ITooltipFlag flag)
    {
        tooltip.add(new TranslationTextComponent("ava.armour.full_equipped"));
        tooltip.add(new TranslationTextComponent("ava.armour.sneak_speed_boost").setStyle(new Style().setColor(TextFormatting.GREEN)));
        tooltip.add(new TranslationTextComponent("ava.armour.uav_warning").setStyle(new Style().setColor(TextFormatting.GREEN)));
        tooltip.add(new TranslationTextComponent("ava.armour.night_vision").setStyle(new Style().setColor(TextFormatting.GREEN)));
        tooltip.add(new TranslationTextComponent("ava.armour.knockback_resistance").setStyle(new Style().setColor(TextFormatting.GREEN)));
        tooltip.add(new TranslationTextComponent("ava.armour.hurt_indicator").setStyle(new Style().setColor(TextFormatting.GREEN)));
    }

    @Override
    public Recipe getRecipe()
    {
        return this.recipe;
    }

    public static void giveTo(PlayerEntity player, boolean eu)
    {
        for (Item item : eu ? EU_ARMOURS : NRF_ARMOURS)
            player.addItemStackToInventory(new ItemStack(item));
    }

    public enum AVAArmourMaterial implements IArmorMaterial
    {
        EU_STANDARD(AVA.MODID + ":eu_standard", 100, new int[]{1, 4, 5, 2}, 0, SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 2.0F, () ->
        {
            return Ingredient.fromItems(Items.LEATHER);
        }),

        NRF_STANDARD(AVA.MODID + ":nrf_standard", 100, new int[]{1, 4, 5, 2}, 0, SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 2.0F, () ->
        {
            return Ingredient.fromItems(Items.LEATHER);
        });

        private static final int[] MAX_DAMAGE_ARRAY = new int[]{25, 25, 25, 25};
        private final String name;
        private final int maxDamageFactor;
        private final int[] damageReductionAmountArray;
        private final int enchantability;
        private final SoundEvent soundEvent;
        private final float toughness;
        private final LazyValue<Ingredient> repairMaterial;

        AVAArmourMaterial(String nameIn, int maxDamageFactorIn, int[] damageReductionAmountsIn, int enchantabilityIn, SoundEvent equipSoundIn, float toughnessIn, Supplier<Ingredient> repairMaterialSupplier)
        {
            this.name = nameIn;
            this.maxDamageFactor = maxDamageFactorIn;
            this.damageReductionAmountArray = damageReductionAmountsIn;
            this.enchantability = enchantabilityIn;
            this.soundEvent = equipSoundIn;
            this.toughness = toughnessIn;
            this.repairMaterial = new LazyValue<>(repairMaterialSupplier);
        }

        public int getDurability(EquipmentSlotType slotIn)
        {
            return MAX_DAMAGE_ARRAY[slotIn.getIndex()] * this.maxDamageFactor;
        }

        public int getDamageReductionAmount(EquipmentSlotType slotIn)
        {
            return this.damageReductionAmountArray[slotIn.getIndex()];
        }

        public int getEnchantability()
        {
            return this.enchantability;
        }

        public SoundEvent getSoundEvent()
        {
            return this.soundEvent;
        }

        public Ingredient getRepairMaterial()
        {
            return this.repairMaterial.getValue();
        }

        @OnlyIn(Dist.CLIENT)
        public String getName()
        {
            return this.name;
        }

        public float getToughness()
        {
            return this.toughness;
        }
    }
}
