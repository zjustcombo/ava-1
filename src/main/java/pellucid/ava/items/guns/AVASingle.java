package pellucid.ava.items.guns;

import pellucid.ava.items.miscs.Recipe;

public class AVASingle extends AVAItemGun
{
    public AVASingle(Properties gunProperties, Recipe recipe)
    {
        super(gunProperties.manual(), recipe);
    }

    /*@Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
        IPlayerAction capability = PlayerAction.getCap((PlayerEntity) entityIn);
        if (this.initTags(stack).getInt("fire") != 0)
            if (capability.isAiming())
                capability.setAiming(false);
    }*/
}
