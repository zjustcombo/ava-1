package pellucid.ava.items.throwables;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.world.World;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.ProjectileEntity;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.entities.throwables.ToxicSmokeGrenadeEntity;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVAWeaponUtil;

public class ToxicSmokeGrenade extends SmokeGrenade
{

    public ToxicSmokeGrenade(Recipe dyes, int[] rgb, String colour)
    {
        this(0.75F, 30 * 20, (EntityType<? extends ToxicSmokeGrenadeEntity>) AVAEntities.M18_TOXIC, new Recipe().addItem(Items.IRON_NUGGET, 3).addItem(Items.SPIDER_EYE, 3).mergeIngredients(dyes), rgb, colour, AVAWeaponUtil.Classification.PROJECTILE);
    }

    public ToxicSmokeGrenade(float power, int range, EntityType<? extends SmokeGrenadeEntity> type, Recipe recipe, int[] rgb, String colour, AVAWeaponUtil.Classification classification)
    {
        super(power, range, type, recipe, rgb, colour, classification);
    }

    @Override
    protected ProjectileEntity getEntity(World world, LivingEntity shooter, ItemStack stack, double velocity, boolean toss)
    {
        return new ToxicSmokeGrenadeEntity((EntityType<? extends ToxicSmokeGrenadeEntity>) AVAEntities.M18_TOXIC, shooter, world, velocity, range, rgb, colour);
    }
}
