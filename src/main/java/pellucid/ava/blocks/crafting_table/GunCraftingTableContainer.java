package pellucid.ava.blocks.crafting_table;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IWorldPosCallable;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.events.CommonModEventBus;

public class GunCraftingTableContainer extends Container
{
    private final IWorldPosCallable worldPosCallable;

    public GunCraftingTableContainer(int id, PlayerInventory playerInventory)
    {
        this(id, playerInventory, IWorldPosCallable.DUMMY);
    }

    public GunCraftingTableContainer(int id, PlayerInventory playerInventory, IWorldPosCallable worldPosCallable)
    {
        super(CommonModEventBus.GUN_CRAFTING_TABLE_CONTAINER, id);
        this.worldPosCallable = worldPosCallable;
        for(int r = 0; r < 3; r++)
            for(int c = 0; c < 9; c++)
                this.addSlot(new Slot(playerInventory, c + r * 9 + 9, 36 + c * 18, 137 + r * 18));
        for(int h = 0; h < 9; ++h)
            this.addSlot(new Slot(playerInventory, h, 36 + h * 18, 195));
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn)
    {
        return isWithinUsableDistance(this.worldPosCallable, playerIn, AVABlocks.GUN_CRAFTING_TABLE);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index)
    {
        return ItemStack.EMPTY;
    }
}
