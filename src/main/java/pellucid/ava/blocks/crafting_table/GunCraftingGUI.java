package pellucid.ava.blocks.crafting_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.AbstractButton;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.*;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.IHasRecipe;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.items.throwables.HandGrenadeItem;
import pellucid.ava.misc.packets.AVAPackets;
import pellucid.ava.misc.packets.CraftMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

@OnlyIn(Dist.CLIENT)
public class GunCraftingGUI extends ContainerScreen<GunCraftingTableContainer>
{
    private static final ResourceLocation GUN_CRAFTING_GUI = new ResourceLocation("ava:textures/gui/gun_crafting_table.png");
    private int selected_tab = 0;
    private int selected_page = 0;
    private Item selected_item = Snipers.MOSIN_NAGANT;
    private HashSet<TabButton> tabButtons = new HashSet<>();
    private HashMap<Integer, ItemButton> itemButtons0 = new HashMap<>();
    private HashMap<Integer, ItemButton> itemButtons1 = new HashMap<>();
    private HashMap<Integer, ItemButton> itemButtons2 = new HashMap<>();
    private HashMap<Integer, ItemButton> itemButtons3 = new HashMap<>();
    private HashMap<Integer, ItemButton> itemButtons4 = new HashMap<>();
    private ArrayList<CraftButton> craftButton = new ArrayList<>();

    public GunCraftingGUI(GunCraftingTableContainer screenContainer, PlayerInventory inv, ITextComponent titleIn)
    {
        super(screenContainer, inv, titleIn);
        this.xSize = 230;
        this.ySize = 219;
        this.selected_tab = 0;
        this.setSelectedPage(0);
    }

    protected void clearAll()
    {
        this.buttons.clear();
        this.children.clear();
        this.tabButtons.clear();
        this.itemButtons0.clear();
        this.itemButtons1.clear();
        this.itemButtons2.clear();
        this.itemButtons3.clear();
        this.itemButtons4.clear();
        this.craftButton.clear();
    }

    @Override
    protected void init()
    {
        super.init();
        clearAll();
        for (int i = 0; i < 5; i++)
        {
            TabButton button = new TabButton(this.guiLeft + 10 + 22 * i, this.guiTop + 7, i);
            if (i==0)
                button.setSelected(true);
            this.tabButtons.add(button);
            this.addButton(button);
        }
        for (int i=0;i<5;i++)
        {
            int c = 0;
            int r = 0;
            int f = 0;
            for (Item item : getItemListFromTab(i))
            {
                ItemButton button = new ItemButton(this.guiLeft + c++ * 18 + 16, this.guiTop + r * 18 + 48, item);
                this.getItemButtonGroup(i).put(f, button);
                button.active = false;
                button.visible = false;
                this.addButton(button);
                if (c == 6)
                {
                    c = 0;
                    r++;
                }
                if (r > 3)
                    r = 0;
                f++;
            }
        }
        this.addButton(new PageButton(this.guiLeft + 60, this.guiTop + 36, true));
        this.addButton(new PageButton(this.guiLeft + 60, this.guiTop + 106, false));
        CraftButton button = new CraftButton(this.guiLeft + 198, this.guiTop + 80, true);
        craftButton.add(button);
        this.addButton(button);
        button = new CraftButton(this.guiLeft + 198, this.guiTop + 111, false);
        craftButton.add(button);
        this.addButton(button);
        this.updatePages();
    }

    protected void updatePages()
    {
        this.resetItemButtons();
        HashMap<Integer, ItemButton> group = this.getItemButtonGroup(this.selected_tab);
        for (int from = this.selected_page * 18; group.size() > from; from++)
        {
            ItemButton button = group.get(from);
            button.visible = true;
            button.active = true;
            button.setSelected(false);
        }
    }

    protected void resetItemButtons()
    {
        for (int i=0;i<5;i++)
        {
            HashMap<Integer, ItemButton> group = this.getItemButtonGroup(i);
            for (int d = 0; d < group.size(); d++)
            {
                ItemButton button = group.get(d);
                button.visible = false;
                button.active = false;
            }
        }
    }

    protected HashMap<Integer, ItemButton> getItemButtonGroup(int tabIndex)
    {
        switch (tabIndex)
        {
            case 0:
            default:
                return this.itemButtons0;
            case 1:
                return this.itemButtons1;
            case 2:
                return this.itemButtons2;
            case 3:
                return this.itemButtons3;
            case 4:
                return this.itemButtons4;
        }
    }
    protected ArrayList<Item> getItemListFromTab()
    {
        return this.getItemListFromTab(this.selected_tab);
    }

    protected ArrayList<Item> getItemListFromTab(int tab)
    {
        ArrayList<Item> list;
        switch (tab)
        {
            case 0:
            default:
                list = new ArrayList<>(Snipers.ITEM_SNIPERS);
                break;
            case 1:
                list = new ArrayList<>(Rifles.ITEM_RIFLES);
                break;
            case 2:
                list = new ArrayList<>(SubmachineGuns.ITEM_SUBMACHINE_GUNS);
                break;
            case 3:
                list = new ArrayList<>(Pistols.ITEM_PISTOLS);
                break;
            case 4:
                list = new ArrayList<>(MiscItems.ITEM_MISCS);
                list.addAll(new ArrayList<>(Projectiles.ITEM_PROJECTILES));
                break;
        }
        list.removeIf(item -> item instanceof Ammo || !(item instanceof IHasRecipe) || (item instanceof AVAItemGun && !((AVAItemGun) item).isMaster()));
        return list;
    }

    protected void onCraft(boolean forGun)
    {
        if (this.selected_item instanceof IHasRecipe)
        {
            Recipe recipe;
            if (this.selected_item instanceof AVAItemGun && !forGun)
                recipe = ((IHasRecipe) ((AVAItemGun) this.selected_item).getMagazineType()).getRecipe();
            else
                recipe = ((IHasRecipe) this.selected_item).getRecipe();
            if (recipe.canCraft(this.playerInventory.player, selected_item))
                AVAPackets.INSTANCE.sendToServer(new CraftMessage(this.selected_item, forGun));
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        if (this.minecraft == null || !(this.selected_item instanceof IHasRecipe))
        {
            this.craftButton.get(0).setItem(Items.AIR);
            return;
        }
        Item item = this.selected_item;
        Recipe recipe = ((IHasRecipe) item).getRecipe();
        this.craftButton.get(0).setItem(item);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.minecraft.getTextureManager().bindTexture(GUN_CRAFTING_GUI);
        int h = 136;
        int x = (this.width - this.xSize) / 2;
        int y = (this.height - this.ySize) / 2;
        this.blit(x, y, 0, 0, this.xSize, this.ySize);
        this.itemRenderer.zLevel = 100.0F;
        this.drawRecipeItem(recipe, item, x + 142, y + 14, true);
        for (Item ingredient : recipe.getIngredients())
        {
            drawRecipeItem(recipe, ingredient, x + h, y + 62, false);
            h += 18;
        }
        if (item instanceof AVAItemGun)
        {
            Item mag = ((AVAItemGun) item).getMagazineType();
            if (mag instanceof IHasRecipe)
            {
                Recipe secRecipe = ((IHasRecipe) mag).getRecipe();
                drawRecipeItem(secRecipe, mag, x + 138, y + 95, true);
                h = 158;
                for (Item ingredient : secRecipe.getIngredients())
                {
                    drawRecipeItem(secRecipe, ingredient, x + h, y + 93, false);
                    h += 18;
                }
            }
            this.craftButton.get(1).setItem(mag);
        }
        else
            this.craftButton.get(1).setItem(Items.AIR);
        this.itemRenderer.zLevel = 0.0F;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        Item item = this.selected_item;
        if (this.minecraft == null || !(this.selected_item instanceof IHasRecipe))
        {
            return;
        }
        Recipe recipe = ((IHasRecipe) item).getRecipe();
        int x = 170;
        int y = 44;
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.pushMatrix();
        RenderSystem.scalef(0.90F, 0.90F, 0.90F);
        drawString(this.font, new TranslationTextComponent(item.getTranslationKey()).getString(), 182, 12, 16762880);
        RenderSystem.popMatrix();
        int i = 22;
        if (recipe.hasDescription())
            for (String text : recipe.getDescription())
            {
                this.drawDescription(text, i);
                i += 8;
            }
        if (item instanceof AVAItemGun)
        {
            AVAItemGun gun = (AVAItemGun) item;
            this.drawAttribute("ava.gui.gun_damage", gun.getDamage(false),              x, y);
            this.drawAttribute("ava.gui.gun_range", gun.getRange(false),                x, y + 10);
            this.drawAttribute("ava.gui.gun_stability", gun.getStability(false),        x, y + 20);
            this.drawAttribute("ava.gui.gun_accuracy", gun.getAccuracy(false),          x + 52, y);
            this.drawAttribute("ava.gui.gun_attack_speed", gun.getAttackSpeed(false),   x + 52, y + 10);
            this.drawAttribute("ava.gui.gun_capacity", gun.getMaxAmmo(),                         x + 52, y + 20);
        }
        else if (item instanceof HandGrenadeItem)
        {
            HandGrenadeItem handGrenadeItem = (HandGrenadeItem) item;
            this.drawAttribute("ava.gui.gun_damage", handGrenadeItem.getDamage(false), x + 4, y);
            this.drawAttribute("ava.gui.gun_range", handGrenadeItem.getRange(),                 x + 4, y + 10);
            this.drawAttribute("ava.gui.gun_capacity", 1,                         x + 4, y + 20);
        }
        for(Widget widget : this.buttons)
            if (widget.isHovered())
            {
                widget.renderToolTip(mouseX - this.guiLeft, mouseY - this.guiTop);
                break;
            }
    }

    protected void drawDescription(String text, int y)
    {
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.pushMatrix();
        RenderSystem.scalef(0.80F, 0.80F, 0.80F);
        drawString(this.font, new TranslationTextComponent(text).getString(), 205, y, 16120058);
        RenderSystem.popMatrix();
    }

    protected void drawAttribute(String attribute, Object value, int x, int y)
    {
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.pushMatrix();
        RenderSystem.scalef(0.80F, 0.80F, 0.80F);
        drawString(this.font, new TranslationTextComponent(attribute).getString() + ": " + value, x, y, 16762880);
        RenderSystem.popMatrix();
    }

    protected void drawRecipeItem(Recipe recipe, Item item, int x, int z, boolean isResult)
    {
        ItemStack stack = new ItemStack(item);
        stack.setCount(isResult ? recipe.getResultCount() : recipe.getCount(item));
        this.itemRenderer.renderItemIntoGUI(stack, x, z);
        this.itemRenderer.renderItemOverlayIntoGUI(this.font, stack, x, z, null);
    }

    protected boolean isPageAvailable(int page)
    {
        return page >= 0 && this.getItemListFromTab().size() / ((page + 1) * 18) > 1;
    }

    protected void setSelectedPage(int index)
    {
        this.selected_page = index;
        this.updatePages();
    }

    protected void setSelectedTab(int index, TabButton button)
    {
        for (TabButton button1 : GunCraftingGUI.this.tabButtons)
            if (button1 != button && button1.isSelected())
                button1.setSelected(false);
        button.setSelected(true);
        this.selected_tab = index;
        this.setSelectedPage(0);
    }

    protected void setSelectedItem(Item item, ItemButton button)
    {
        for (int i=0;i<5;i++)
        {
            HashMap<Integer, ItemButton> group = this.getItemButtonGroup(i);
            for (int d = 0; d < group.size(); d++)
            {
                ItemButton button1 = group.get(d);
                if (button1 != button && button1.isSelected())
                    button1.setSelected(false);
            }
        }
        button.setSelected(true);
        this.selected_item = item;
    }

    @OnlyIn(Dist.CLIENT)
    class TabButton extends AbstractButton
    {
        private boolean selected;
        private String name;
        private Item item;
        private int type;

        public TabButton(int xIn, int yIn, int type)
        {
            super(xIn, yIn, 22, 22, "");
            this.type = type;
            switch (type)
            {
                case 0:
                default:
                    this.item = Snipers.MOSIN_NAGANT.getItem();
                    this.name = "snipers";
                    break;
                case 1:
                    this.item = Rifles.M4A1.getItem();
                    this.name = "rifles";
                    break;
                case 2:
                    this.item = SubmachineGuns.X95R.getItem();
                    this.name = "submachine_guns";
                    break;
                case 3:
                    this.item = Pistols.P226.getItem();
                    this.name = "pistols";
                    break;
                case 4:
                    this.item = MiscItems.NRF_STANDARD_KEVLAR.getItem();
                    this.name = "miscs";
                    break;
            }
        }

        @Override
        public void onPress()
        {
            GunCraftingGUI.this.setSelectedTab(this.type, this);
        }

        @Override
        public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_)
        {
            Minecraft.getInstance().getTextureManager().bindTexture(GUN_CRAFTING_GUI);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            int x = 0;
            if (this.selected || this.isHovered())
                x += this.width;
            this.blit(this.x, this.y, x, 219, this.width, this.height);
            ItemRenderer renderer = GunCraftingGUI.this.itemRenderer;
            renderer.zLevel = 100.0F;
            renderer.renderItemIntoGUI(new ItemStack(this.item), this.x + 3, this.y + 3);
            renderer.zLevel = 0.0F;
        }

        @Override
        public void renderToolTip(int p_renderToolTip_1_, int p_renderToolTip_2_)
        {
            GunCraftingGUI.this.renderTooltip(new StringTextComponent(I18n.format("ava.gui.tab." + this.name)).getString(), p_renderToolTip_1_, p_renderToolTip_2_);
        }

        public boolean isSelected()
        {
            return this.selected;
        }

        public void setSelected(boolean selectedIn)
        {
            this.selected = selectedIn;
        }
    }

    @OnlyIn(Dist.CLIENT)
    class ItemButton extends AbstractButton
    {
        private boolean selected;
        private Item item;

        public ItemButton(int xIn, int yIn, Item item)
        {
            super(xIn, yIn, 18, 18, "");
            this.item = item;
        }

        @Override
        public void onPress()
        {
            GunCraftingGUI.this.setSelectedItem(this.item, this);
        }

        @Override
        public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_)
        {
            if (this.isSelected())
            {
                Minecraft.getInstance().getTextureManager().bindTexture(GUN_CRAFTING_GUI);
                RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
                this.blit(this.x + 2, this.y + 2, 45, 229, this.width, this.height);
            }
            ItemRenderer renderer = Minecraft.getInstance().getItemRenderer();
            renderer.zLevel = 100.0F;
            renderer.renderItemIntoGUI(new ItemStack(this.item), this.x + 2, this.y + 2);
            renderer.zLevel = 0.0F;
        }

        @Override
        public void renderToolTip(int p_renderToolTip_1_, int p_renderToolTip_2_)
        {
            GunCraftingGUI.this.renderTooltip(new StringTextComponent(I18n.format(this.item.getTranslationKey())).getString(), p_renderToolTip_1_, p_renderToolTip_2_);
        }

        public boolean isSelected()
        {
            return this.selected;
        }

        public void setSelected(boolean selectedIn)
        {
            this.selected = selectedIn;
        }
    }

    @OnlyIn(Dist.CLIENT)
    class PageButton extends AbstractButton
    {
        private boolean up;
        private boolean isAvailable;

        public PageButton(int xIn, int yIn, boolean up)
        {
            super(xIn, yIn, 22, 11, "");
            this.up = up;
        }

        @Override
        public void onPress()
        {
            if (this.isAvailable)
                GunCraftingGUI.this.setSelectedPage(getPage());
        }

        @Override
        public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_)
        {
            this.isAvailable = GunCraftingGUI.this.isPageAvailable(getPage());
            Minecraft.getInstance().getTextureManager().bindTexture(GUN_CRAFTING_GUI);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            int x = this.up ? 176 : 110;
            if (this.isAvailable)
            {
                x += this.width;
                if (this.isHovered())
                    x += this.width;
            }
            this.blit(this.x, this.y, x, 219, this.width, this.height);
        }

        private int getPage()
        {
            return GunCraftingGUI.this.selected_page + (this.up ? -1 : + 1);
        }
    }

    @OnlyIn(Dist.CLIENT)
    class CraftButton extends AbstractButton
    {
        //for magazine/ammo
        private boolean forGun;
        private boolean selected;
        private Item item = Items.AIR;

        public CraftButton(int xIn, int yIn, boolean forGun)
        {
            super(xIn, yIn, 22, 9, "");
            this.forGun = forGun;
        }

        @Override
        public void onPress()
        {
            if (this.active)
            {
                GunCraftingGUI.this.onCraft(this.forGun);
                this.selected = true;
            }
        }

        @Override
        public void onRelease(double p_onRelease_1_, double p_onRelease_3_)
        {
            if (this.active)
                this.selected = false;
        }

        public void setItem(Item item)
        {
            this.item = item;
        }

        @Override
        public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_)
        {
            this.active = (this.item != Items.AIR) && ((IHasRecipe) this.item).getRecipe().canCraft(GunCraftingGUI.this.playerInventory.player, GunCraftingGUI.this.selected_item);
            Minecraft.getInstance().getTextureManager().bindTexture(GUN_CRAFTING_GUI);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            int x = 44;
            if (this.active && this.isHovered())
                x += this.width;
            if (this.active && this.selected)
                x += this.width;
            if (!this.active)
                x += this.width * 2;
            this.blit(this.x, this.y, x, 219, this.width, this.height);
            drawCenteredString(GunCraftingGUI.this.font, "Craft", this.x + this.width / 2, this.y, this.active ? 54783 : 16711680);
        }
    }
}
