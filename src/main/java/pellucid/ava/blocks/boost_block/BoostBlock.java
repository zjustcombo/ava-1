package pellucid.ava.blocks.boost_block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalBlock;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import pellucid.ava.blocks.AVABlocks;

public class BoostBlock extends HorizontalBlock
{
    private static final VoxelShape SHAPE = Block.makeCuboidShape(0.0F, 0.0F, 0.0F, 16.0F, 8.0F, 16.0F);

    public BoostBlock()
    {
        super(Properties.create(Material.IRON).notSolid().lightValue(5).hardnessAndResistance(3.0F, 3.0F));
        setDefaultState(getDefaultState().with(HORIZONTAL_FACING, Direction.NORTH));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
    {
        builder.add(HORIZONTAL_FACING);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context)
    {
        return getDefaultState().with(HORIZONTAL_FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        return SHAPE;
    }

//    @Override
//    public void onEntityCollision(BlockState state, World worldIn, BlockPos pos, Entity entityIn)
//    {
//        if (!(entityIn instanceof PlayerEntity) || worldIn.isRemote() || worldIn.getGameTime() % 20L != 0)
//            return;
//        PlayerEntity player = (PlayerEntity) entityIn;
//        if (player.experienceLevel >= 1)
//        {
//            IPlayerAction cap = PlayerAction.getCap(player);
//            IParticleData particle;
//            String type;
//            TextFormatting colour;
//            boolean succeed = true;
//            int level;
//            if (this == AVABlocks.ATTACK_DAMAGE_BOOST_BLOCK)
//            {
//                int boost = cap.getAttackDamageBoost();
//                if (boost > 19)
//                    succeed = false;
//                else
//                    cap.setAttackDamageBoost(cap.getAttackDamageBoost() + 1);
//                particle = ParticleTypes.CRIT;
//                type = "attack damage";
//                colour = TextFormatting.RED;
//                level = cap.getAttackDamageBoost();
//            }
//            else
//            {
//                int boost = cap.getHealthBoost();
//                if (boost > 19)
//                    succeed = false;
//                else
//                    cap.setHealthBoost(cap.getHealthBoost() + 1);
//                particle = ParticleTypes.HAPPY_VILLAGER;
//                type = "health";
//                colour = TextFormatting.GREEN;
//                level = cap.getHealthBoost();
//            }
//            if (succeed)
//            {
//                player.addExperienceLevel(-1);
//                player.sendMessage(new StringTextComponent("your " + type + " boost is increased by 1, currently on level " + level).setStyle(Style.EMPTY.setColor(Color.fromTextFormatting(colour))), player.getUniqueID());
//                if (worldIn instanceof ServerWorld)
//                {
//                    Random rand = new Random();
//                    for (int i = 0; i < 15; i++)
//                        ((ServerWorld) worldIn).spawnParticle(particle, pos.getX() + rand.nextFloat(), pos.getY() + rand.nextFloat() * 2, pos.getZ() + rand.nextFloat(), 1, 0.0F, 0.0F, 0.0F, 0.0F);
//                }
//                worldIn.playSound(null, pos.getX(), pos.getY(), pos.getZ(), AVASounds.BLOCK_BOOSTS_PLAYER, SoundCategory.BLOCKS, 1.0F, 1.0F);
//            }
//        }
//    }

    @Override
    public boolean hasTileEntity(BlockState state)
    {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world)
    {
        return new BoostTileEntity(state.getBlock() == AVABlocks.ATTACK_DAMAGE_BOOST_BLOCK);
    }
}
