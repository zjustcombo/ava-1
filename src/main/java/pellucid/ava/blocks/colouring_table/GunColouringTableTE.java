package pellucid.ava.blocks.colouring_table;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import pellucid.ava.events.CommonModEventBus;

import javax.annotation.Nullable;

public class GunColouringTableTE extends TileEntity implements INamedContainerProvider
{
    public GunColouringTableTE()
    {
        super(CommonModEventBus.GUN_COLOURING_TABLE_TE);
    }

    public GunColouringTableTE(TileEntityType<? extends GunColouringTableTE> type)
    {
        super(type);
    }

    @Override
    public ITextComponent getDisplayName()
    {
        return new TranslationTextComponent("ava.container.gun_colouring_table");
    }

    @Nullable
    @Override
    public Container createMenu(int p_createMenu_1_, PlayerInventory p_createMenu_2_, PlayerEntity p_createMenu_3_)
    {
        return this.world == null ? null : new GunColouringTableContainer(p_createMenu_1_, p_createMenu_2_, IWorldPosCallable.of(this.world, this.getPos()));
    }
}
