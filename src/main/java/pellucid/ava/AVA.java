package pellucid.ava;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pellucid.ava.misc.cap.AVACapabilities;
import pellucid.ava.misc.config.AVAClientConfig;
import pellucid.ava.misc.config.AVAServerConfig;
import pellucid.ava.misc.packets.AVAPackets;

@Mod("ava")
public class AVA
{
    public static final String MODID = "ava";
    public static final Logger LOGGER = LogManager.getLogger(MODID);

    public AVA()
    {
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, AVAClientConfig.build(new ForgeConfigSpec.Builder()));
        ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, AVAServerConfig.build(new ForgeConfigSpec.Builder()));
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        bus.addListener(this::setup);
    }

    private void setup(final FMLCommonSetupEvent event)
    {
        AVAPackets.registerAll(true);
        AVACapabilities.registerAll();
    }
}
