package pellucid.ava.misc.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.IHasRecipe;
import pellucid.ava.items.miscs.Recipe;

import java.util.function.Supplier;

public class CraftMessage
{
    private Item item;
    private boolean forGun;

    public CraftMessage(Item item, boolean forGun)
    {
        this.item = item;
        this.forGun = forGun;
    }

    public static void encode(CraftMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeItemStack(new ItemStack(msg.item)).writeBoolean(msg.forGun);
    }

    public static CraftMessage decode(PacketBuffer packetBuffer)
    {
        return new CraftMessage(packetBuffer.readItemStack().getItem(), packetBuffer.readBoolean());
    }

    public static void handle(CraftMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() ->
        {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
                if (message.item instanceof IHasRecipe)
                {
                    Item item = message.forGun ? message.item : ((AVAItemGun) message.item).getMagazineType();
                    Recipe recipe = ((IHasRecipe) item).getRecipe();
                    if (recipe.canCraft(player, item))
                        recipe.craft(player, item);
                }
        });
        ctx.get().setPacketHandled(true);
    }
}
