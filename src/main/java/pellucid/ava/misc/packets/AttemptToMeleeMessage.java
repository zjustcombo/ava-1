package pellucid.ava.misc.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.miscs.MeleeWeapon;

import java.util.function.Supplier;

public class AttemptToMeleeMessage
{
    private final boolean left;
    public AttemptToMeleeMessage(boolean left)
    {
        this.left = left;
    }

    public static void encode(AttemptToMeleeMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeBoolean(msg.left);
    }

    public static AttemptToMeleeMessage decode(PacketBuffer packetBuffer)
    {
        return new AttemptToMeleeMessage(packetBuffer.readBoolean());
    }

    public static void handle(AttemptToMeleeMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                ItemStack stack = player.getHeldItemMainhand();
                if (stack.getItem() instanceof MeleeWeapon)
                {
                    MeleeWeapon knife = (MeleeWeapon) stack.getItem();
                    if (knife.canMelee(stack))
                        knife.preMelee(stack, message.left);
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
