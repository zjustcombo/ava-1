package pellucid.ava.misc.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.function.Supplier;

public class PlaySoundMessage
{
    private String sound;

    public PlaySoundMessage(String sound)
    {
        this.sound = sound;
    }

    public static void encode(PlaySoundMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeString(msg.sound);
    }

    public static PlaySoundMessage decode(PacketBuffer packetBuffer)
    {
        return new PlaySoundMessage(packetBuffer.readString(32767));
    }

    public static void handle(PlaySoundMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
                player.getServerWorld().playMovingSound(null, player, ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(message.sound)), SoundCategory.PLAYERS, player.isSprinting() ? 1.0F : 0.75F, 1.0F);
        });
        ctx.get().setPacketHandled(true);
    }

}
