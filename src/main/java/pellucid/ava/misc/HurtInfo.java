package pellucid.ava.misc;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;

import java.util.ArrayList;

public class HurtInfo
{
    private ArrayList<Info> info = new ArrayList<>();

    public void add(Info info)
    {
        this.info.add(info);
    }

    public ArrayList<Info> getInfo()
    {
        return info;
    }

    public void update()
    {
        info.removeIf(info -> --info.ticksLeft <= 0);
    }

    public static class Info
    {
        public final Vec3d attackerPos;
        public int ticksLeft = 60;

        public Info(Entity attacker)
        {
            this(attacker.getPositionVec());
        }

        public Info(Vec3d attackerPos)
        {
            this.attackerPos = attackerPos;
        }
    }
}
