package pellucid.ava.misc.renderers.models.m4a1;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class M4a1Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(2, 6, 0),
            new Vector3f(0.25F, 1.25F, -3F),
            new Vector3f(4, 4, 1.45F)
    );

    private static final float[] RUN_ROTATION = new float[]{-27, 55, 36};
    private static final float[] RUN_SCALE = new float[]{2.5F, 3, 2};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-11.75F, 1.75F, 1}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-12.75F, 1, 1}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>()
    {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-14, 1.5F, 1}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD_1 = new Perspective(new float[]{41, 2, 0}, new float[]{-3.0F, 11.25F, -0.25F}, new float[]{3.0F, 3.0F, 2.25F});
    private static final Perspective RELOAD_2 = new Perspective(new float[]{14, 2, -7}, new float[]{-3.0F, 6.0F, 0.0F}, new float[]{3, 3, 1.65F});
    private static final Perspective RELOAD_3 = new Perspective(new float[]{27, 2, -6}, new float[]{-2.25F, 7.5F, 0.0F}, new float[]{3, 3, 1.65F});
    private static final Perspective RELOAD_4 = new Perspective(new float[]{4, 12, -24}, new float[]{-1.5F, 3.75F, -1.75F}, new float[]{3, 3.1F, 1.65F});
    private static final Perspective RELOAD_5 = new Perspective(new float[]{18, -29, 28}, new float[]{-1.75F, 4.75F, -3.0F}, new float[]{2.8F, 2.8F, 2});
    private static final Perspective RELOAD_6 = new Perspective(new float[]{42, -14, 32}, new float[]{-4.5F, 7F, 3F}, new float[]{3F, 3, 2.1F});
    private static final Perspective RELOAD_7 = new Perspective(new float[]{42, -29, 37}, new float[]{-0.25F, 8.5F, -3F}, new float[]{3F, 3, 2.1F});
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>()
    {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(5, RELOAD_1));
        add(new Animation(8, RELOAD_2));
        add(new Animation(13, RELOAD_3));
        add(new Animation(28, RELOAD_4));
        add(new Animation(33, RELOAD_3));
        add(new Animation(38, RELOAD_5));
        add(new Animation(40, RELOAD_6));
        add(new Animation(44, RELOAD_7));
        add(new Animation(51, ORIGINAL_FP_RIGHT));
        add(new Animation(56, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective AIMING = new Perspective(fArr(0), new float[]{-9.005F, 4.517F, 9F}, new float[]{4, 4, 0.35F});

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:m4a1/m4a1_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:m4a1/m4a1_slide#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:m4a1/m4a1_fire#inventory");

    public M4a1Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getFireQuads(state, rand));
        quads.addAll(getMagazineQuads());
        quads.addAll(getSlideQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    @Override
    public Perspective getAimingPosition()
    {
        return AIMING;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getSlideQuads()
    {
        return get(getSlide(), (newQuad) ->
        {
            if (!translateQuadTo(newQuad, 3.5F, reload, 35, 40, Direction.SOUTH))
                translateQuadTo(newQuad, 3.5F, reload, 40, 42, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) ->
        {
            if (!translateQuadTo(newQuad, 4.0F, reload, 7, 13, Direction.DOWN))
                if (!translateQuad(newQuad, -4.0F, reload, 13, 17, Direction.UP))
                    translateQuadFrom(newQuad, 4.0F, reload, 17, 23, Direction.UP);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2.5F, 5.75F, 4.5F);
                scale = new Vector3f(2.3F, 2.3F, 1.95F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, -3.5F, 0);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.25F, 1, 0);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(-2.25F, 2.75F, -1.5F);
                scale = v3f(3);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}