package pellucid.ava.misc.renderers.models.m4a1;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class M4A1DreamcatcherModel extends M4a1Model
{
    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:m4a1/m4a1_dreamcatcher_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:m4a1/m4a1_dreamcatcher_slide#inventory");

    public M4A1DreamcatcherModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
    }
}