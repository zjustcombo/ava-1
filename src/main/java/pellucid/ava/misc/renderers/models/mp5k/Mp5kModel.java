package pellucid.ava.misc.renderers.models.mp5k;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Mp5kModel extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(2, 5, 0),
            new Vector3f(-4.75F, 3.5F, 4.75F),
            new Vector3f(1, 0.95F, 0.55F)
    );

    private static final float[] RUN_ROTATION = new float[]{-35, 47, 43};
    private static final float[] RUN_SCALE = new float[]{0.7F, 0.7F, 0.7F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-8, 4, 6.5F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-8.5F, 3.5F, 6.5F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-9.25F, 4, 6.5F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final float[] RELOAD_SCALE = new float[]{0.7F, 0.8F, 0.4F};
    private static final Perspective RELOAD_1 = new Perspective(new float[]{43, 2, -42}, new float[]{-2.75F, 5.75F, 5.5F}, RELOAD_SCALE);
    private static final Perspective RELOAD_2 = new Perspective(new float[]{15, 2, -3}, new float[]{-3.5F, 4.75F, 5.75F}, RELOAD_SCALE);
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(4, RELOAD_1));
        add(new Animation(20, RELOAD_1));
        add(new Animation(25, RELOAD_2));
        add(new Animation(30, RELOAD_2));
        add(new Animation(35, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{0, 0, 40}, new float[]{0.15F, -0.6F, 0}, new float[]{0.5F, 1, 0.5F});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 40}, new float[]{0.15F, -0.4F, -0.2F}, new float[]{0.5F, 1, 0.5F});
    private static final Perspective LEFT_HAND_RELOADING_2_FP = new Perspective(new float[]{-40, 0, 40}, new float[]{0, -0.25F, 0.1F}, new float[]{0.5F, 1, 0.5F});
    private static final Perspective LEFT_HAND_RELOADING_3_FP = new Perspective(new float[]{-40, 0, 40}, new float[]{0, -0.3F, 0.1F}, new float[]{0.5F, 1, 0.5F});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(6, LEFT_HAND_RELOADING_FP));
        add(new Animation(10, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(14, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(18, LEFT_HAND_RELOADING_FP));
        add(new Animation(20, LEFT_HAND_RELOADING_FP));
        add(new Animation(24, LEFT_HAND_RELOADING_2_FP));
        add(new Animation(28, LEFT_HAND_RELOADING_3_FP));
        add(new Animation(24, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(35, LEFT_HAND_ORIGINAL_FP));
    }};

    private static final Perspective AIMING = new Perspective(fArr(0), new float[]{-9F, 4.837F, 10.0F}, new float[]{1.0F, 0.95F, 0.55F});

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:mp5k/mp5k_magazine#inventory");
    public static final ModelResourceLocation HANDLE = new ModelResourceLocation("ava:mp5k/mp5k_handle#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:mp5k/mp5k_fire#inventory");

    public Mp5kModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }


    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getFireQuads(state, rand));
        quads.addAll(getMagazineQuads());
        quads.addAll(getHandleQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getHandle()
    {
        return HANDLE;
    }

    @Override
    public Perspective getAimingPosition()
    {
        return AIMING;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getHandleQuads()
    {
        return get(getHandle(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 2.0F, reload, 25, 31, Direction.SOUTH))
                translateQuadFrom(newQuad, 2.0F, reload, 31, 33, Direction.NORTH);
            else translateQuad(newQuad, 2.0F, fire, 1, Integer.MAX_VALUE, Direction.SOUTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (translateQuadTo(newQuad, 6.0F, reload, 6, 11, Direction.DOWN))
                translateQuadTo(newQuad, 3.5F, reload, 6, 11, Direction.SOUTH);
            else if (translateQuad(newQuad, 6.0F,reload, 11, 15, Direction.DOWN))
                translateQuad(newQuad, 3.5F, reload, 11, 15, Direction.SOUTH);
            else if (translateQuadFrom(newQuad, 6.0F, reload, 15, 20, Direction.UP))
                translateQuadFrom(newQuad, 3.5F, reload, 15, 20, Direction.NORTH);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(HANDLE);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2, 3.5F, 4.25F);
                scale = new Vector3f(1, 1, 0.8F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, 90);
                translation = new Vector3f(0, -3, -0.75F);
                scale = new Vector3f(0.55F, 0.55F, 0.5F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.5F, 0, 0);
                scale = new Vector3f(0.55F, 0.55F, 0.55F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(1.5F, 0, -1.5F);
                scale = new Vector3f(1.25F, 1.25F, 1.15F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
