package pellucid.ava.misc.renderers.models.mp5sd5;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Mp5sd5Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(-3, 3, 0),
            new Vector3f(-6F, 4.5F, 3.75F),
            new Vector3f(0.95F, 0.85F, 0.45F)
    );

    private static final float[] RUN_ROTATION = new float[]{-33, 71, 64};
    private static final float[] RUN_SCALE = new float[]{0.45F, 0.6F, 0.45F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-9.25F, 5, 6.5F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-9.75F, 4.5F, 6.5F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-10.25F, 5, 6.5F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final float[] RELOAD_SCALE = new float[]{0.95F, 0.75F, 0.5F};
    private static final Perspective RELOAD_1 = new Perspective(new float[]{42, 19, -44}, new float[]{-4.25F, 6.75F, 5}, RELOAD_SCALE);
    private static final Perspective RELOAD_2 = new Perspective(new float[]{40, 9, -44}, new float[]{-4.25F, 7.25F, 5}, RELOAD_SCALE);
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(3, RELOAD_1));
        add(new Animation(10, RELOAD_1));
        add(new Animation(15, RELOAD_2));
        add(new Animation(25, RELOAD_2));
        add(new Animation(30, RELOAD_1));
        add(new Animation(32, RELOAD_1));
        add(new Animation(35, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{0, 0, 40}, new float[]{0.15F, -0.6F, 0}, new float[]{0.5F, 1, 0.5F});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 40}, new float[]{0.15F, -0.4F, -0.2F}, new float[]{0.5F, 1, 0.5F});
    private static final Perspective LEFT_HAND_RELOADING_2_FP = new Perspective(new float[]{-40, 0, 40}, new float[]{0.025F, -0.25F, -0.1F}, new float[]{0.5F, 1, 0.5F});
    private static final Perspective LEFT_HAND_RELOADING_3_FP = new Perspective(new float[]{-40, 0, 40}, new float[]{0.025F, -0.3F, -0.1F}, new float[]{0.5F, 1, 0.5F});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(6, LEFT_HAND_RELOADING_FP));
        add(new Animation(10, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(14, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(18, LEFT_HAND_RELOADING_FP));
        add(new Animation(20, LEFT_HAND_RELOADING_FP));
        add(new Animation(24, LEFT_HAND_RELOADING_2_FP));
        add(new Animation(28, LEFT_HAND_RELOADING_3_FP));
        add(new Animation(24, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(35, LEFT_HAND_ORIGINAL_FP));
    }};

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:mp5sd5/mp5sd5_magazine#inventory");
    public static final ModelResourceLocation HANDLE = new ModelResourceLocation("ava:mp5sd5/mp5sd5_handle#inventory");

    public Mp5sd5Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(this.getFireQuads(state, rand));
        quads.addAll(this.getMagazineQuads());
        quads.addAll(this.getHandleQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getHandle()
    {
        return HANDLE;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return null;
    }

    protected List<BakedQuad> getHandleQuads()
    {
        return get(getHandle(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 2.0F, reload, 25, 31, Direction.SOUTH))
                translateQuadFrom(newQuad, 2.0F, reload, 31, 33, Direction.NORTH);
            else translateQuad(newQuad, 2.0F, fire, 1, Integer.MAX_VALUE, Direction.SOUTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (translateQuadTo(newQuad, 6.0F, reload, 6, 11, Direction.DOWN))
                translateQuadTo(newQuad, 2.0F, reload, 6, 11, Direction.SOUTH);
            else if (translateQuad(newQuad, -6.0F, reload, 11, 15, Direction.UP))
                translateQuad(newQuad, -2.0F, reload, 11, 15, Direction.NORTH);
            else if (translateQuadFrom(newQuad, 6.0F, reload, 15, 20, Direction.UP))
                translateQuadFrom(newQuad, 2.0F, reload, 15, 20, Direction.NORTH);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(HANDLE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-1.75F, 5.5F, 3.75F);
                scale = v3f(1);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, -3, -0.5F);
                scale = v3f(0.55F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.75F, 0, -1.25F);
                scale = v3f(0.45F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.75F, 0, 0);
                scale = v3f(1);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
