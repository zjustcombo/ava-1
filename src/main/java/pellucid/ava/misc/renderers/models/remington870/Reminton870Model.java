package pellucid.ava.misc.renderers.models.remington870;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Reminton870Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(-3, 3, 0),
            new Vector3f(-6.25F, 5.75F, 4.5F),
            new Vector3f(1F, 1.05F, 0.5F)
    );

    private static final float[] RUN_ROTATION = new float[]{45, 83, -10};
    private static final float[] RUN_SCALE = new float[]{0.25F, 0.25F, 0.25F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-8.5F, 7.75F, 10}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-8.75F, 7.5F, 10}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-9, 7.75F, 10}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final float[] RELOAD_SCALE = new float[]{0.85F, 1.05F, 1};
    private static final Perspective RELOAD_1 = new Perspective(new float[]{61, 8, -24}, new float[]{-4, 8.25F, 3.75F}, RELOAD_SCALE);
    private static final Perspective RELOAD_2 = new Perspective(new float[]{55, 8, -1}, new float[]{-4.25F, 8.25F, 3.75F}, RELOAD_SCALE);
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RELOAD_1));
        add(new Animation(16, RELOAD_2));
        add(new Animation(19, RELOAD_1));
    }};

    private static final Perspective FIRE_1 = new Perspective(new float[]{26, -3, -3}, new float[]{-5.25F, 7.75F, 6}, new float[]{0.85F, 1.05F, 0.65F});
    private static final Perspective FIRE_2 = new Perspective(new float[]{30, -3, -3}, new float[]{-5.25F, 7.75F, 6.25F}, new float[]{0.85F, 1.05F, 0.65F});
    protected static final ArrayList<Animation> FIRE_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, FIRE_1));
        add(new Animation(5, FIRE_2));
        add(new Animation(9, ORIGINAL_FP_RIGHT));
    }};

    private static final float[] LEFT_HAND_RUN_ROTATION = new float[]{0, -10, -45};
    private static final float[] LEFT_HAND_RUN_SCALE = new float[]{0.35F, 0.45F, 1};
    private static final Perspective LEFT_HAND_RUN_FP_1 = new Perspective(LEFT_HAND_RUN_ROTATION, new float[]{-0.1F, -0.05F, -0.25F}, LEFT_HAND_RUN_SCALE);
    private static final Perspective LEFT_HAND_RUN_FP_2 = new Perspective(LEFT_HAND_RUN_ROTATION, new float[]{-0.05F, -0.05F, -0.2F}, LEFT_HAND_RUN_SCALE);
    public static final ArrayList<Animation> LEFT_HAND_RUN_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_RUN_FP_1));
        add(new Animation(3, LEFT_HAND_RUN_FP_2));
        add(new Animation(6, new Perspective(LEFT_HAND_RUN_ROTATION, new float[]{0, -0.05F, -0.25F}, LEFT_HAND_RUN_SCALE)));
        add(new Animation(9, LEFT_HAND_RUN_FP_2));
        add(new Animation(12, LEFT_HAND_RUN_FP_1));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{10, 0, 35}, new float[]{0.25F, -0.65F, 0}, new float[]{0.5F, 1, 0.5F});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{10, 0, 35}, new float[]{-0.15F, -0.35F, -0.25F}, new float[]{0.5F, 1, 0.5F});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
        add(new Animation(8, LEFT_HAND_RELOADING_FP));
        add(new Animation(19, LEFT_HAND_ORIGINAL_FP));
    }};

    public static final ModelResourceLocation BULLET = new ModelResourceLocation("ava:remington870/remington870_bullet#inventory");
    public static final ModelResourceLocation FOREARM = new ModelResourceLocation("ava:remington870/remington870_forearm#inventory");

    public Reminton870Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }


    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getBulletQuads());
        quads.addAll(getForearmQuads());
        return quads;
    }

    protected ModelResourceLocation getBullet()
    {
        return BULLET;
    }

    protected ModelResourceLocation getForearm()
    {
        return FOREARM;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return null;
    }

    protected List<BakedQuad> getForearmQuads()
    {
        return get(getForearm(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 2.0F, fire, 2, 7, Direction.SOUTH))
                translateQuadFrom(newQuad, 2.0F, fire, 7, 12, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getBulletQuads()
    {
        return get(getBullet(), (newQuad) -> {
            if (translateQuadFrom(newQuad, 3.5F, reload, 3, 10, Direction.UP))
                translateQuadFrom(newQuad, 1.0F, reload, 3, 10, Direction.NORTH);
        }, reload < 3 || reload >= 10);
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(BULLET);
        ModelLoader.addSpecialModel(FOREARM);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2, 5.5F, 4.75F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(-180, 0, 90);
                scale = v3f(0.5F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.5F, 0, 0);
                scale = v3f(0.5F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.25F, 0.75F, 0);
                scale = v3f(1.65F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    public void modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemCameraTransforms.TransformType type)
    {
        if (this.fire != 0)
        {
            Perspective perspective3f = getPerspectiveInBetween(FIRE_ANIMATION, this.fire);
            rotation.set(perspective3f.getRotation());
            translation.set(perspective3f.getTranslation());
            scale.set(perspective3f.getScale());
        }
        super.modifyPerspective(rotation, translation, scale, type, false);
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
