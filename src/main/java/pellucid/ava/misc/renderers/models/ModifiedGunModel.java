package pellucid.ava.misc.renderers.models;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.AVA;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.misc.cap.PlayerAction;
import pellucid.ava.misc.config.AVAClientConfig;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.BakedModel;
import pellucid.ava.misc.renderers.Perspective;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.*;

public abstract class ModifiedGunModel extends BakedModel
{
    public static final float[] ONE_A = fArr(1);
    public static final Vector3f ONE_V = v3f(1);

    public static final ResourceLocation FIRE_TEXTURE_0 = new ResourceLocation(AVA.MODID, "item/gun_effects/fire");
    //        public static final ResourceLocation FIRE_TEXTURE_1 = new ResourceLocation("item/gun_effects/fire_1");
    public static final ResourceLocation FIRE_TEXTURE_2 = new ResourceLocation(AVA.MODID, "item/gun_effects/fire2");
    public static final ResourceLocation FIRE_TEXTURE_3 = new ResourceLocation(AVA.MODID, "item/gun_effects/fire3");

    public static final ResourceLocation FIRE_TEXTURE_0_GREEN = new ResourceLocation(AVA.MODID, "item/gun_effects/fire_green");
    public static final ResourceLocation FIRE_TEXTURE_2_GREEN = new ResourceLocation(AVA.MODID, "item/gun_effects/fire2_green");
    public static final ResourceLocation FIRE_TEXTURE_3_GREEN = new ResourceLocation(AVA.MODID, "item/gun_effects/fire3_green");

    protected int fire;
    protected int reload;
    protected int run;
    private boolean aiming;

    public ModifiedGunModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
        CompoundNBT compound = ((AVAItemGun) stack.getItem()).initTags(stack);
        this.fire = compound.getInt("fire");
        this.reload = compound.getInt("reload");
        this.run = compound.getInt("run");
        if (entity instanceof PlayerEntity)
        {
            PlayerEntity player = (PlayerEntity) entity;
            this.aiming = player.isAlive() && PlayerAction.getCap(player).isAiming();
        }
    }

    protected abstract ModelResourceLocation getFireModel();

    protected List<BakedQuad> getFireQuads(@Nullable BlockState state, Random rand)
    {
        if (getFireModel() == null || fire == 0)
            return Collections.emptyList();
        IBakedModel fire = Minecraft.getInstance().getModelManager().getModel(getFireModel());
        List<BakedQuad> fireQuads = new ArrayList<>();
        for (BakedQuad quad : fire.getQuads(state, null, rand))
        {
            if (ModelLoader.instance() == null)
                break;
//            TextureAtlasSprite sprite = getFireSprite(quad.getSprite());
            fireQuads.add(copyQuad(quad));
        }
        return fireQuads;
    }

//    protected TextureAtlasSprite getFireSprite(TextureAtlasSprite old)
//    {
//        return RAND.nextBoolean() ? old.getAtlasTexture().getTextureLocation() == FIRE_TEXTURE_0 ? getSprite(FIRE_TEXTURE_2_GREEN) : getSprite(FIRE_TEXTURE_3_GREEN) : old;
//    }

    protected TextureAtlasSprite getSprite(ResourceLocation loc)
    {
        return Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(loc);
    }

    public static void addTextureAtlasSprite(TextureStitchEvent.Pre event)
    {
        event.addSprite(FIRE_TEXTURE_0);
        event.addSprite(FIRE_TEXTURE_2);
        event.addSprite(FIRE_TEXTURE_3);
        event.addSprite(FIRE_TEXTURE_0_GREEN);
        event.addSprite(FIRE_TEXTURE_2_GREEN);
        event.addSprite(FIRE_TEXTURE_3_GREEN);
    }

    public void modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemCameraTransforms.TransformType type)
    {
        modifyPerspective(rotation, translation, scale, type, true);
    }

    public void modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemCameraTransforms.TransformType type, boolean shouldTiltDefault)
    {
        if (type == GUI || type == NONE && (this.run != 0 || this.aiming))
            return;
        if (type != THIRD_PERSON_LEFT_HAND && type != THIRD_PERSON_RIGHT_HAND)
        {
            boolean isPlayer = entity instanceof PlayerEntity;
            if (this.fire == 0)
            {
                if (this.run != 0 && isPlayer && AVAClientConfig.FP_RUN_ANIMATION.get())
                    copy(getPerspectiveInBetween(this.getRunAnimation(), this.run), rotation, translation, scale);
                else if (this.reload != 0 && AVAClientConfig.FP_RELOAD_ANIMATION.get())
                    copy(getPerspectiveInBetween(this.getReloadAnimation(), this.reload), rotation, translation, scale);
                else if (isPlayer)
                {
                    PlayerEntity player = Minecraft.getInstance().player;
                    if (player != null)
                        bob(rotation, translation, player.ticksExisted);
                }
            }
            else if (shouldTiltDefault && AVAClientConfig.FP_FIRE_ANIMATION.get())
                fireTilt(rotation, translation);
            if (isPlayer)
            {
                PlayerEntity player = (PlayerEntity) entity;
                if (this.aiming)
                {
                    Perspective perspective = Perspective.copy(getAimingPosition());
                    if (perspective != null)
                    {
                        //offsets:
                        perspective.rotation.add(0.0F, 0.435F, 0.0F);
                        perspective.translation.add(0.035F, 0.0075F, 0);
                        copy(perspective, rotation, translation, scale);
                    }
                }
                else if (player.isAlive() && !player.onGround && AVAClientConfig.FP_JUMP_ANIMATION.get())
                    translation.add(0.0F, 2.0F, 0.0F);
            }
        }
        else if (entity instanceof AVAHostileEntity && entity.getHeldItemMainhand().getItem() instanceof AVAItemGun)
            ((AVAHostileEntity) entity).modifyItemModel(rotation, translation, scale);
    }

    public Perspective getAimingPosition()
    {
        return null;
    }

    protected boolean fireTilt(Vector3f rotation, Vector3f translation)
    {
        return fireTilt(rotation, translation, 3, 1.25F);
    }

    protected boolean fireTilt(Vector3f rotation, Vector3f translation, float rotX, float tsltZ)
    {
        boolean shouldTilt = this.fire != 0;
        if (shouldTilt)
        {
            rotation.add(rotX, 0.0F, 0.0F);
            translation.add(0.0F, 0.0F, tsltZ);
        }
        return shouldTilt;
    }

    protected abstract ArrayList<Animation> getRunAnimation();

    protected abstract ArrayList<Animation> getReloadAnimation();
}
