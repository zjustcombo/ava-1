package pellucid.ava.misc.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Matrix4f;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import pellucid.ava.AVA;
import pellucid.ava.events.AVAClientEvent;
import pellucid.ava.items.miscs.Binocular;
import pellucid.ava.misc.HurtInfo;
import pellucid.ava.misc.KillTips;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.WorldData;
import pellucid.ava.misc.config.AVAClientConfig;

import java.util.ArrayList;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class AVARenderer extends AVAClientEvent
{
    public static final ResourceLocation BLACK = new ResourceLocation(AVA.MODID + ":textures/overlay/black.png");
    public static final ResourceLocation WHITE = new ResourceLocation(AVA.MODID + ":textures/overlay/white.png");
    public static final ResourceLocation CROSSHAIR = new ResourceLocation(AVA.MODID + ":textures/overlay/crosshair.png");
    public static final ResourceLocation NIGHT_VISION = new ResourceLocation(AVA.MODID + ":textures/overlay/night_vision.png");
    public static final ResourceLocation BATTERY = new ResourceLocation(AVA.MODID + ":textures/overlay/battery.png");
    public static final ResourceLocation BINOCULAR = new ResourceLocation(AVA.MODID + ":textures/overlay/binocular.png");
    public static final ResourceLocation HURT_INDICATOR = new ResourceLocation(AVA.MODID + ":textures/overlay/hurt_indicator.png");

    @SubscribeEvent
    public static void onRenderHUD(RenderGameOverlayEvent.Pre event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player == null || !player.isAlive())
            return;
        if (event.getType() == RenderGameOverlayEvent.ElementType.CROSSHAIRS && isAvailable(player) && minecraft.isGameFocused() && minecraft.gameSettings.thirdPersonView == 0 && minecraft.currentScreen == null)
        {
            event.setCanceled(true);
            if (!isAiming(player) && getGun(player).hasCrosshair() && !player.isSprinting() && getGun(player).initTags(getHeldStack(player)).getInt("reload") == 0 && WorldData.getCap(player.getEntityWorld()).shouldRenderCrosshair() && AVAClientConfig.SHOULD_RENDER_CROSS_HAIR.get())
            {
                int screenHeight = event.getWindow().getScaledHeight();
                int screenWidth = event.getWindow().getScaledWidth();
                float multiplier = getGun(player).getMultiplier(player);
                float spread = getGun(player).spread(cap(player), player, multiplier, false);
                float initialAccuracy = 0;
                minecraft.getTextureManager().bindTexture(CROSSHAIR);
                Tessellator tessellator = Tessellator.getInstance();
                for (int i=-1;i<3;i+=2)
                {
                    drawTransparent(true);
                    RenderSystem.color4f(AVAClientConfig.RED.get().floatValue(), AVAClientConfig.GREEN.get().floatValue(), AVAClientConfig.BLUE.get().floatValue(), AVAClientConfig.TRANSPARENCY.get().floatValue());
                    double s = screenHeight / 2.0D - (((spread + initialAccuracy) * 15 - 2.0F) - (i==-1?1:0) + 2) * i;
                    double h = s - 5 * i;
                    double w = screenWidth / 2.0D;
                    BufferBuilder bufferbuilder = tessellator.getBuffer();
                    bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
                    bufferbuilder.pos(w - 1, i == -1 ? s : h, -90.0D).tex(0.0F, 1.0F).endVertex();
                    bufferbuilder.pos(w - 1, i == -1 ? h : s, -90.0D).tex(0.0F, 0.0F).endVertex();
                    bufferbuilder.pos(w, i == -1 ? h : s, -90.0D).tex(1.0F, 0.0F).endVertex();
                    bufferbuilder.pos(w, i == -1 ? s : h, -90.0D).tex(1.0F, 1.0F).endVertex();
                    tessellator.draw();
//                    Screen.fill(event.getMatrixStack(), (int) (w - 1), (int) h, (int) w, (int) s, 0xBF33ff00);
//                    Screen.fill(stack, event.getWindow().getScaledWidth() - 120, y - 2, event.getWindow().getScaledWidth() - 2, y + 12, 0x8CFFFFFF);
                    drawTransparent(false);
                }
                for (int i=-1;i<3;i+=2)
                {
                    drawTransparent(true);
                    RenderSystem.color4f(AVAClientConfig.RED.get().floatValue(), AVAClientConfig.GREEN.get().floatValue(), AVAClientConfig.BLUE.get().floatValue(), 1.0F);
                    double s = screenWidth / 2.0D - (((spread + initialAccuracy) * 15 - 2.0F) - (i==-1?1:0) + 2) * i;
                    double w = s - 5 * i;
                    double h = screenHeight / 2.0D;
                    tessellator = Tessellator.getInstance();
                    BufferBuilder bufferbuilder = tessellator.getBuffer();
                    bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
                    bufferbuilder.pos(i == -1 ? w : s, h - 1, -90.0D).tex(0.0F, 1.0F).endVertex();
                    bufferbuilder.pos(i == -1 ? s : w, h - 1, -90.0D).tex(0.0F, 0.0F).endVertex();
                    bufferbuilder.pos(i == -1 ? s : w, h, -90.0D).tex(1.0F, 0.0F).endVertex();
                    bufferbuilder.pos(i == -1 ? w : s, h, -90.0D).tex(1.0F, 1.0F).endVertex();
                    tessellator.draw();
//                    Screen.fill(event.getMatrixStack(), (int) w, (int) (h - 1), (int) s, (int) h, 0xBF33ff00);
                    drawTransparent(false);
                }
            }
        }
    }

    @SubscribeEvent
    public static void onRenderOverlay(RenderGameOverlayEvent.Post event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player == null || !player.isAlive())
            return;
        int screenWidth = event.getWindow().getScaledWidth();
        int screenHeight = event.getWindow().getScaledHeight();
        MatrixStack stack = new MatrixStack();
        if (event.getType() == RenderGameOverlayEvent.ElementType.ALL)
        {
            IPlayerAction capability = cap(player);
            int duration = capability.getFlashDuration();
            if (capability.getFlashDuration() > 0)
            {
                drawTransparent(true);
                minecraft.getTextureManager().bindTexture(WHITE);
                RenderSystem.color4f(1.0F, 1.0F, 1.0F, Math.min(duration / 20.0F, 1.0F));
                fillScreen(screenWidth, screenHeight);
                drawTransparent(false);
            }
            if (capability.isNightVisionActivated())
            {
                drawTransparent(true);
                RenderSystem.color4f(1.0F, 1.0F, 1.0F, 0.25F);
                minecraft.getTextureManager().bindTexture(NIGHT_VISION);
                fillScreen(screenWidth, screenHeight);
                minecraft.getTextureManager().bindTexture(BATTERY);
                int x = screenWidth / 2 - 16;
                int y = screenHeight / 4 * 3;
                Tessellator tessellator = Tessellator.getInstance();
                BufferBuilder bufferbuilder = tessellator.getBuffer();
                bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
                bufferbuilder.pos(x, y + 32, -90.0D).tex(0.0F, 1.0F).endVertex();
                bufferbuilder.pos(x + 32, y + 32, -90.0D).tex(1.0F, 1.0F).endVertex();
                bufferbuilder.pos(x + 32, y, -90.0D).tex(1.0F, 0.0F).endVertex();
                bufferbuilder.pos(x, y, -90.0D).tex(0.0F, 0.0F).endVertex();
                tessellator.draw();
                drawTransparent(false);
                int battery = capability.getNightVisionBattery();
                for (int i = 0; i < 6; i++)
                    Screen.fill(stack.getLast().getMatrix(), x + 10, y + 2 + 5 * i, x + 22, y + 6 + 5 * i, battery >= (5 - i) * 100 ? 0x80FFFFFF : 0x40AAAAAA);
            }
            if (minecraft.gameSettings.thirdPersonView == 0)
            {
                minecraft.getTextureManager().bindTexture(HURT_INDICATOR);
                drawTransparent(true);
                for (HurtInfo.Info info : new ArrayList<>(capability.getHurtInfo().getInfo()))
                {
                    float yaw = player.rotationYaw % 360;
                    float q = 90;
                    boolean xp = info.attackerPos.x - player.getPosX() < 0;
                    boolean zp = info.attackerPos.z > player.getPosZ();
                    if (xp && zp)
                        q *= 0;
                    else if (xp)
                        q *= 1;
                    else if (!zp)
                        q *= 2;
                    else
                        q *= 3;
                    double z = Math.abs(info.attackerPos.z - player.getPosZ());
                    double x = Math.abs(info.attackerPos.x - player.getPosX());
                    float direction;
                    if (q == 0 || q == 180)
                        direction = (float) (Math.atan2(x, z));
                    else
                        direction = (float) (Math.atan2(z, x));
                    direction = (float) (direction * 180 / Math.PI) + q;
                    float angle = Math.abs(direction - yaw);
                    if (direction < yaw) angle = 360 - angle;
                    stack.push();
                    stack.translate(screenWidth / 2.0F, screenHeight / 2.0F, 0.0F);
                    stack.rotate(Vector3f.ZP.rotationDegrees(angle));
                    stack.translate(-screenWidth / 2.0F, -screenHeight / 2.0F, 0.0F);
                    Matrix4f matrix = stack.getLast().getMatrix();
                    Tessellator tessellator = Tessellator.getInstance();
                    BufferBuilder bufferbuilder = tessellator.getBuffer();
                    bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
                    float w = screenWidth / 4.0F;
                    bufferbuilder.pos(matrix, w, screenHeight / 2.0F + w, -90.0F).tex(0.0F, 1.0F).endVertex();
                    bufferbuilder.pos(matrix, w * 3, screenHeight / 2.0F + w, -90.0F).tex(1.0F, 1.0F).endVertex();
                    bufferbuilder.pos(matrix, w * 3, screenHeight / 2.0F - w, -90.0F).tex(1.0F, 0.0F).endVertex();
                    bufferbuilder.pos(matrix, w, screenHeight / 2.0F - w, -90.0F).tex(0.0F, 0.0F).endVertex();
                    tessellator.draw();
                    stack.pop();
                }
                drawTransparent(false);
            }
        }
        if (event.getType() == RenderGameOverlayEvent.ElementType.ALL && !player.isSpectator() && minecraft.isGameFocused() && minecraft.gameSettings.thirdPersonView == 0 && minecraft.currentScreen == null && getHeldStack(player).getItem() instanceof Binocular)
        {
            if (!((Binocular) getHeldStack(player).getItem()).initTags(getHeldStack(player)).getBoolean("aiming"))
                return;
            drawTransparent(true);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            fillCentredMaxWithFilledSides(screenWidth, screenHeight, BINOCULAR);
            drawTransparent(false);
//            BlockRayTraceResult blockResult = player.world.rayTraceBlocks(new RayTraceContext(player.getPositionVec().add(0.0F, player.getEyeHeight(), 0.0F), player.getLookVec().scale(200.0F), RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.NONE, null);
//            double distance = player.getPositionVec().add(0.0F, player.getEyeHeight(), 0.0F).distanceTo(obj.getHitVec());
//            if (obj.getType() == RayTraceResult.Type.MISS)
//                player.world.rayTraceBlocks()
            int distance = getHeldStack(player).getOrCreateTag().getInt("hit");
            drawCenteredString(minecraft.fontRenderer, String.valueOf(distance), (int) (minecraft.getMainWindow().getScaledWidth() / 2.0F), (int) (minecraft.getMainWindow().getScaledHeight() / 2.0F + 15), 0xFFDFDFDF);
        }
        if (event.getType() == RenderGameOverlayEvent.ElementType.ALL && isAvailable(player) && minecraft.isGameFocused() && minecraft.gameSettings.thirdPersonView == 0 && minecraft.currentScreen == null)
        {
            if (isAiming(player))
            {
                ResourceLocation texture = getGun(player).getScopeType().getTexture();
                if (texture != null)
                {
                    drawTransparent(true);
                    fillCentredMaxWithFilledSides(screenWidth, screenHeight, texture);
                    drawTransparent(false);
                }
            }
        }
        if (event.getType() == RenderGameOverlayEvent.ElementType.ALL && player.isAlive())
        {
            ArrayList<KillTips.KillTip> tips = new ArrayList<>(cap(player).getKillTips().getTips());
            int y = event.getWindow().getScaledHeight() / 30;
            for (KillTips.KillTip tip : tips)
            {
                int x = event.getWindow().getScaledWidth() - 5;
                stack.push();
                stack.translate(0.0F, 0.0F, -100.0F);
                Screen.fill(event.getWindow().getScaledWidth() - 120, y - 2, event.getWindow().getScaledWidth() - 2, y + 12, 0x8CFFFFFF);
                stack.pop();
                PlayerEntity vic = tip.getPlayer();
                ITextComponent name = vic.getDisplayName();
                minecraft.fontRenderer.drawString(name.getString(), x -= minecraft.fontRenderer.getStringWidth(name.getString()), y, getColourForName(vic, player));
                minecraft.getItemRenderer().renderItemIntoGUI(new ItemStack(tip.getWeapon()), x -= 20, y - 3);
                if (tip.hasKiller())
                {
                    PlayerEntity killer = tip.getKiller();
                    name = killer.getDisplayName();
                    minecraft.fontRenderer.drawString(name.getString(), x - minecraft.fontRenderer.getStringWidth(name.getString()) - 3, y, getColourForName(killer, player));
                }
                y += 14;
            }
        }
    }

    @SubscribeEvent
    public static void FOVUpdate(FOVUpdateEvent event)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player == null || !player.isAlive())
            return;
        float fov = event.getFov();
        if (isAvailable(player) && isAiming(player))
            event.setNewfov(fov - getGun(player).getScopeType().getMagnification());
        if (isFullEquipped(player) && player.isSneaking() && player.onGround)
            event.setNewfov(event.getNewfov() - 0.375F);
        ItemStack stack = getHeldStack(player);
        if (stack.getItem() instanceof Binocular && ((Binocular) stack.getItem()).initTags(stack).getBoolean("aiming"))
            event.setNewfov(event.getNewfov() - 0.30F);
    }

    @SubscribeEvent
    public static void RenderHand(RenderHandEvent event)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player == null || !player.isAlive())
            return;
        ItemStack stack = getHeldStack(player);
        if ((isAvailable(player) && isAiming(player) && getGun(player).getScopeType().getTexture() != null) || (stack.getItem() instanceof Binocular && ((Binocular) stack.getItem()).initTags(stack).getBoolean("aiming")))
            event.setCanceled(true);
    }

    @SubscribeEvent
    public static void onRenderFogColour(EntityViewRenderEvent.FogColors event)
    {
        PlayerEntity player = Minecraft.getInstance().player;
        if (player == null || !player.isAlive())
            return;
        if (cap(player).isNightVisionActivated())
        {
            float red = event.getRed();
            float green = event.getGreen();
            float blue = event.getBlue();
            float f10 = Math.min(1.0F / red, Math.min(1.0F / green, 1.0F / blue));
            if (Float.isInfinite(f10))
                f10 = Math.nextAfter(f10, 0.0);
            red = red * f10;
            green = green * f10;
            blue = blue * f10;
            event.setRed(red);
            event.setGreen(green);
            event.setBlue(blue);
        }
    }
}
