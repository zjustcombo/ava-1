package pellucid.ava.misc.renderers.meleemodels;

import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import pellucid.ava.misc.config.AVAClientConfig;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.BakedModel;

import javax.annotation.Nullable;
import java.util.ArrayList;

import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.*;

public abstract class ModifiedMeleeWeaponModel extends BakedModel
{
    protected int meleeL;
    protected int meleeR;

    public ModifiedMeleeWeaponModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
        CompoundNBT compound = stack.getOrCreateTag();
        meleeL = compound.getInt("meleeL");
        meleeR = compound.getInt("meleeR");
    }

    public void modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemCameraTransforms.TransformType type)
    {
        if (type == GUI || type == NONE)
            return;
        boolean isPlayer = entity instanceof PlayerEntity;
        boolean thirdPerson = type == THIRD_PERSON_LEFT_HAND || type == THIRD_PERSON_RIGHT_HAND;
        if (isPlayer)
        {
            PlayerEntity player = (PlayerEntity) entity;
            boolean attacking = meleeL > 0 || meleeR > 0;
            if (meleeL > 0)
                copy(getPerspectiveInBetween(getLeftAnimation(), meleeL), rotation, translation, scale);
            else if (meleeR > 0)
                copy(getPerspectiveInBetween(getRightAnimation(), meleeR), rotation, translation, scale);
            if (!thirdPerson && !attacking)
            {
                if (player.isAlive() && !player.onGround && AVAClientConfig.FP_JUMP_ANIMATION.get())
                    translation.add(0.0F, 2.0F, 0.0F);
                else if (player.isAlive())
                    bob(rotation, translation, player.ticksExisted);
            }
        }
    }

    protected abstract ArrayList<Animation> getLeftAnimation();

    protected abstract ArrayList<Animation> getRightAnimation();
}
