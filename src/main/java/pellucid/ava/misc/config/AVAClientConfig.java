package pellucid.ava.misc.config;

import net.minecraftforge.common.ForgeConfigSpec;
import pellucid.ava.AVA;

import static net.minecraftforge.common.ForgeConfigSpec.*;

public class AVAClientConfig extends AVAConfig
{
    public static BooleanValue SHOULD_RENDER_CROSS_HAIR;
    public static DoubleValue RED;
    public static DoubleValue GREEN;
    public static DoubleValue BLUE;
    public static DoubleValue TRANSPARENCY;

    public static BooleanValue FP_BOBBING;
    public static BooleanValue FP_RUN_ANIMATION;
    public static BooleanValue FP_JUMP_ANIMATION;
    public static BooleanValue FP_FIRE_ANIMATION;
    public static BooleanValue FP_RELOAD_ANIMATION;

    public static IntValue PRESET_CHOICE;

    public static ConfigValue<String> PRIMARY_WEAPON_1;
    public static ConfigValue<String> SECONDARY_WEAPON_1;
    public static ConfigValue<String> MELEE_WEAPON_1;
    public static ConfigValue<String> PROJECTILE_1_1;
    public static ConfigValue<String> PROJECTILE_1_2;
    public static ConfigValue<String> PROJECTILE_1_3;
    public static ConfigValue<String> SPECIAL_WEAPON_1;

    public static ConfigValue<String> PRIMARY_WEAPON_2;
    public static ConfigValue<String> SECONDARY_WEAPON_2;
    public static ConfigValue<String> MELEE_WEAPON_2;
    public static ConfigValue<String> PROJECTILE_2_1;
    public static ConfigValue<String> PROJECTILE_2_2;
    public static ConfigValue<String> PROJECTILE_2_3;
    public static ConfigValue<String> SPECIAL_WEAPON_2;

    public static ConfigValue<String> PRIMARY_WEAPON_3;
    public static ConfigValue<String> SECONDARY_WEAPON_3;
    public static ConfigValue<String> MELEE_WEAPON_3;
    public static ConfigValue<String> PROJECTILE_3_1;
    public static ConfigValue<String> PROJECTILE_3_2;
    public static ConfigValue<String> PROJECTILE_3_3;
    public static ConfigValue<String> SPECIAL_WEAPON_3;

    public static ForgeConfigSpec build(Builder builder)
    {
        BUILDER = builder;
        SHOULD_RENDER_CROSS_HAIR = builder.define(AVA.MODID + ".crosshair.config.enable", true);
        RED = builder.defineInRange(AVA.MODID + ".crosshair.config.red", 0.2F, 0, 1);
        GREEN = builder.defineInRange(AVA.MODID + ".crosshair.config.green", 1.0F, 0, 1);
        BLUE = builder.defineInRange(AVA.MODID + ".crosshair.config.blue", 0.0F, 0, 1);
        TRANSPARENCY = builder.defineInRange(AVA.MODID + ".crosshair.config.transparency", 1.0F, 0, 1);

        FP_BOBBING = builder.define(AVA.MODID + ".fp_animation.config.enable_bobbing", true);
        FP_RUN_ANIMATION = builder.define(AVA.MODID + ".fp_animation.config.enable_run", true);
        FP_JUMP_ANIMATION = builder.define(AVA.MODID + ".fp_animation.config.enable_jump", true);
        FP_FIRE_ANIMATION = builder.define(AVA.MODID + ".fp_animation.config.enable_fire", true);
        FP_RELOAD_ANIMATION = builder.define(AVA.MODID + ".fp_animation.config.enable_reload", true);
        makeSpace(5);
        PRESET_CHOICE = builder.defineInRange("Your preset choice", 1, 1, 3);
        makeSpace(3, "Preset 1");
        PRIMARY_WEAPON_1 = builder.define("Primary Weapon", "mp5k");
        SECONDARY_WEAPON_1 = builder.define("Secondary Weapon", "p226");
        MELEE_WEAPON_1 = builder.define("Melee Weapon", "field_knife");
        PROJECTILE_1_1 = builder.define("Projectile 1", "m18_grey");
        PROJECTILE_1_2 = builder.define("Projectile 2", "m116a1");
        PROJECTILE_1_3 = builder.define("Projectile 3", "m67");
        SPECIAL_WEAPON_1 = builder.define("Special Weapon", "binocular");
        makeSpace(3, "Preset 2");
        PRIMARY_WEAPON_2 = builder.define("Primary Weapon2", "mp5k");
        SECONDARY_WEAPON_2 = builder.define("Secondary Weapon2", "p226");
        MELEE_WEAPON_2 = builder.define("Melee Weapon2", "field_knife");
        PROJECTILE_2_1 = builder.define("Projectile2 1", "m18_grey");
        PROJECTILE_2_2 = builder.define("Projectile2 2", "m116a1");
        PROJECTILE_2_3 = builder.define("Projectile2 3", "m67");
        SPECIAL_WEAPON_2 = builder.define("Special Weapon2", "binocular");
        makeSpace(3, "Preset 3");
        PRIMARY_WEAPON_3 = builder.define("Primary Weapon3", "mp5k");
        SECONDARY_WEAPON_3 = builder.define("Secondary Weapon3", "p226");
        MELEE_WEAPON_3 = builder.define("Melee Weapon3", "field_knife");
        PROJECTILE_3_1 = builder.define("Projectile3 1", "m18_grey");
        PROJECTILE_3_2 = builder.define("Projectile3 2", "m116a1");
        PROJECTILE_3_3 = builder.define("Projectile3 3", "m67");
        SPECIAL_WEAPON_3 = builder.define("Special Weapon3", "binocular");
        return builder.build();
    }
}
