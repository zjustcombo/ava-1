package pellucid.ava.misc.config;

import net.minecraftforge.common.ForgeConfigSpec;

import static net.minecraftforge.common.ForgeConfigSpec.*;

public class AVAServerConfig extends AVAConfig
{
    public static DoubleValue M67_EXPLOSIVE_DAMAGE;
    public static DoubleValue M67_EXPLOSIVE_RANGE;

    public static DoubleValue M116A1_FLASH_RANGE;
    public static IntValue  M116A1_FLASH_DURATION;

    public static DoubleValue MK3A2_EXPLOSIVE_DAMAGE;
    public static DoubleValue MK3A2_EXPLOSIVE_RANGE;
    public static DoubleValue MK3A2_FLASH_RANGE;
    public static IntValue MK3A2_FLASH_DURATION;

    public static DoubleValue M202_ROCKET_EXPLOSIVE_DAMAGE;
    public static DoubleValue M202_ROCKET_EXPLOSIVE_RANGE;

    public static DoubleValue GM94_GRENADE_EXPLOSIVE_DAMAGE;
    public static DoubleValue GM94_GRENADE_EXPLOSIVE_RANGE;

    public static BooleanValue IS_AMMO_KIT_SUPPLIER_CRAFTABLE;
    public static BooleanValue EXPLOSION_DESTROYS_ENTITIES;
    public static IntValue EXPLOSION_DESTRUCTION_TYPE;
    public static IntValue BIONCULAR_RANGE_UPDATE_INTERVAL;
    public static DoubleValue DAMAGE_MULTIPLIER_AGAINST_PLAYERS;
    public static DoubleValue DAMAGE_MULTIPLIER_AGAINST_OTHERS;
    public static BooleanValue SHOULD_GUARDS_SPAWN;
    public static IntValue GUARDS_SPAWN_CHANCE;

    public static BooleanValue ENABLE_SNIPERS;
    public static BooleanValue ENABLE_SEMI_SNIPERS;
    public static BooleanValue ENABLE_RIFLES;
    public static BooleanValue ENABLE_SUB_MACHINEGUNS;
    public static BooleanValue ENABLE_SHOTGUNS;
    public static BooleanValue ENABLE_PISTOLS;
    public static BooleanValue ENABLE_AUTO_PISTOLS;
    public static BooleanValue ENABLE_PROJECTILES;
    public static BooleanValue ENABLE_SPECIAL_WEAPONS;

    public static BooleanValue COMPETITIVE_MODE;
    public static BooleanValue COMPETITIVE_MODE_2;

    private static final double MAX_EXPLOSIVE_DAMAGE = 100.0D;
    private static final double MAX_EXPLOSIVE_RANGE = 30.0D;
    private static final double MAX_FLASH_RANGE = 50.0D;
    private static final int MAX_FLASH_DURATION = 200;

    public static ForgeConfigSpec build(Builder builder)
    {
        BUILDER = builder;
        M67_EXPLOSIVE_DAMAGE = builder.defineInRange("m67_explosive_damage", 26.0D, 0.0D, MAX_EXPLOSIVE_DAMAGE);
        M67_EXPLOSIVE_RANGE = builder.defineInRange("m67_explosive_range", 3.0D, 0.0D, MAX_EXPLOSIVE_RANGE);
        makeSpace();
        M116A1_FLASH_RANGE = builder.defineInRange("m116a1_flash_range", 5.0D, 0.0D, MAX_FLASH_RANGE);
        M116A1_FLASH_DURATION = builder.defineInRange("m116a1_flash_duration", 80, 0, MAX_FLASH_DURATION);
        makeSpace();
        MK3A2_EXPLOSIVE_DAMAGE = builder.defineInRange("mk3a2_explosive_damage", 26.0D, 0.0D, MAX_EXPLOSIVE_DAMAGE);
        MK3A2_EXPLOSIVE_RANGE = builder.defineInRange("mk3a2_explosive_range", 2.0D, 0.0D, MAX_FLASH_RANGE);
        MK3A2_FLASH_RANGE = builder.defineInRange("mk3a2_flash_range", 2.0D, 0.0D, MAX_EXPLOSIVE_RANGE);
        MK3A2_FLASH_DURATION = builder.defineInRange("mk3a2_flash_duration", 60, 0, MAX_FLASH_DURATION);
        makeSpace();
        M202_ROCKET_EXPLOSIVE_DAMAGE = builder.defineInRange("m202_explosive_damage", 20.0D, 0.0D, MAX_EXPLOSIVE_DAMAGE);
        M202_ROCKET_EXPLOSIVE_RANGE = builder.defineInRange("m202_explosive_range", 6.0D, 0.0D, MAX_EXPLOSIVE_RANGE);
        makeSpace();
        GM94_GRENADE_EXPLOSIVE_DAMAGE = builder.defineInRange("gm94_explosive_damage", 18.0D, 0.0D, MAX_EXPLOSIVE_DAMAGE);
        GM94_GRENADE_EXPLOSIVE_RANGE = builder.defineInRange("gm94_explosive_range", 5.0D, 0.0D, MAX_EXPLOSIVE_RANGE);
        makeSpace();
        IS_AMMO_KIT_SUPPLIER_CRAFTABLE = builder.define("is_ammo_kit_supplier_craftable", false);
        EXPLOSION_DESTROYS_ENTITIES = builder.define("explosion_destroys_entities", false);
        builder.comment("0: None, 1: Glass Material Only, 2: All Blocks");
        EXPLOSION_DESTRUCTION_TYPE = builder.defineInRange("explosion_destruction_type", 0, 0, 2);
        builder.comment("Ticks of binoculars' range detecting update interval, lower values might cause lags");
        BIONCULAR_RANGE_UPDATE_INTERVAL = builder.defineInRange("binocular_update_interval", 4, 0, 40);
        builder.comment("Multiplier for the damage caused by all ava damage source");
        DAMAGE_MULTIPLIER_AGAINST_PLAYERS = builder.defineInRange("damage_multiplier_against_players", 2.5D, 0, 40.0D);
        DAMAGE_MULTIPLIER_AGAINST_OTHERS = builder.defineInRange("damage_multiplier_against_others", 1.0D, 0, 40.0D);
        SHOULD_GUARDS_SPAWN = builder.define("should_guards_spawn", false);
        GUARDS_SPAWN_CHANCE = builder.defineInRange("guards_spawn_chance", 50, 0, 100);
        makeSpace(2, "https://docs.google.com/document/d/1950eqPMlePlyoeaFH1_MRRBRw5nishlXjR3h9FkDIhE/edit?usp=sharing", "Whether guns like m24 and mosin-nagant can be used");
        ENABLE_SNIPERS = builder.define("enable_snipers", true);
        builder.comment("Whether guns like sr-25 can be used");
        ENABLE_SEMI_SNIPERS = builder.define("enable_semi_snipers", true);
        builder.comment("Whether guns like mk20 can be used");
        ENABLE_RIFLES = builder.define("enable_rifles", true);
        builder.comment("Whether guns like mp5k and x95r can be used");
        ENABLE_SUB_MACHINEGUNS = builder.define("enable_sub_machineguns", true);
        builder.comment("Whether guns like remington870 can be used");
        ENABLE_SHOTGUNS = builder.define("enable_shotguns", true);
        builder.comment("Whether guns like p226 can be used");
        ENABLE_PISTOLS = builder.define("enable_pistols", true);
        builder.comment("Whether guns like N/A can be used");
        ENABLE_AUTO_PISTOLS = builder.define("enable_auto_pistols", true);
        builder.comment("Whether projectiles like m67 and m18 can be used");
        ENABLE_PROJECTILES = builder.define("enable_projectiles", true);
        builder.comment("Whether special weapons like gm94 and m202 can be used");
        ENABLE_SPECIAL_WEAPONS = builder.define("enable_special_weapons", true);
        makeSpace(5, "Set both to true to enable competitive mode, please make sure you know what it is" +
                "before setting it to true");
        COMPETITIVE_MODE = builder.define("enable_competitive_mode", false);
        COMPETITIVE_MODE_2 = builder.define("enable_competitive_mode_confirm", false);
        return builder.build();
    }

    public static boolean isCompetitiveModeActivated()
    {
        return COMPETITIVE_MODE.get() && COMPETITIVE_MODE_2.get();
    }
}
