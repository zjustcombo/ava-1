package pellucid.ava.misc;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.AVA;

public class AVASounds
{
    public static final SoundEvent DEFAULT_AIM = get("default_aim");
    public static final SoundEvent DEFAULT_PULL_GRENADE = get("default_pull_grenade");
    public static final SoundEvent DEFAULT_FOOTSTEP = get("default_footstep");

    public static final SoundEvent NIGHT_VISION_ACTIVATE = get("night_vision_activate");
    public static final SoundEvent UAV_CAPTURED = get("uav_captured");
    public static final SoundEvent UAV_CAPTURES = get("uav_captures");

    public static final SoundEvent AMMO_SUPPLIER_CONSUME = get("ammo_kit_supplier_consume");

    public static final SoundEvent FOOTSTEP_WOOD = get("footstep_wood");
    public static final SoundEvent FOOTSTEP_METAL = get("footstep_metal");
    public static final SoundEvent FOOTSTEP_SAND = get("footstep_sand");
    public static final SoundEvent FOOTSTEP_SOLID = get("footstep_solid");
    public static final SoundEvent FOOTSTEP_GRASS = get("footstep_grass");
    public static final SoundEvent FOOTSTEP_FLOOD = get("footstep_flood");
    public static final SoundEvent FOOTSTEP_WOOL = get("footstep_wool");

    public static final SoundEvent GRENADE_HIT = get("grenade_hit");

    public static final SoundEvent COLLECTS_PICKABLE = get("collects_pickable");

    public static final SoundEvent BULLET_FLIES_BY = get("bullet_flies_by");

    public static final SoundEvent GENERIC_GRENADE_EXPLODE = get("generic_grenade_explode");
    public static final SoundEvent FLASH_GRENADE_EXPLODE = get("flash_grenade_explode");
    public static final SoundEvent ROCKET_EXPLODE = get("rocket_explode");
    public static final SoundEvent ROCKET_TRAVEL = get("rocket_travel");
    public static final SoundEvent SMOKE_GRENADE_ACTIVE = get("smoke_grenade_active");

    public static final SoundEvent M4A1_SHOOT = get("m4a1_shoot");
    public static final SoundEvent M4A1_RELOAD = get("m4a1_reload");
    public static final SoundEvent MOSIN_NAGANT_SHOOT = get("mosin_nagant_shoot");
    public static final SoundEvent MOSIN_NAGANT_RELOAD = get("mosin_nagant_reload");
    public static final SoundEvent P226_SHOOT = get("p226_shoot");
    public static final SoundEvent P226_RELOAD = get("p226_reload");
    public static final SoundEvent X95R_SHOOT = get("x95r_shoot");
    public static final SoundEvent X95R_RELOAD = get("x95r_reload");
    public static final SoundEvent MK20_SHOOT = get("mk20_shoot");
    public static final SoundEvent MK20_RELOAD = get("mk20_reload");
    public static final SoundEvent M24_SHOOT = get("m24_shoot");
    public static final SoundEvent M24_RELOAD = get("m24_reload");
    public static final SoundEvent REMINGTON870_SHOOT = get("remington870_shoot");
    public static final SoundEvent REMINGTON870_RELOAD = get("remington870_reload");
    public static final SoundEvent FN_FNC_SHOOT = get("fn_fnc_shoot");
    public static final SoundEvent FN_FNC_RELOAD = get("fn_fnc_reload");
    public static final SoundEvent XM8_SHOOT = get("xm8_shoot");
    public static final SoundEvent XM8_RELOAD = get("xm8_reload");
    public static final SoundEvent MP5SD5_SHOOT = get("mp5sd5_shoot");
    public static final SoundEvent MP5SD5_RELOAD = get("mp5sd5_reload");
    public static final SoundEvent MK18_SHOOT = get("mk18_shoot");
    public static final SoundEvent MK18_RELOAD = get("mk18_reload");
    public static final SoundEvent SR_25_SHOOT = get("sr_25_shoot");
    public static final SoundEvent SR_25_RELOAD = get("sr_25_reload");
    public static final SoundEvent FG42_SHOOT = get("fg42_shoot");
    public static final SoundEvent FG42_RELOAD = get("fg42_reload");
    public static final SoundEvent MP5K_SHOOT = get("mp5k_shoot");
    public static final SoundEvent MP5K_RELOAD = get("mp5k_reload");
    public static final SoundEvent M202_SHOOT = get("m202_shoot");
    public static final SoundEvent M202_RELOAD = get("m202_reload");
    public static final SoundEvent GM94_SHOOT = get("gm94_shoot");
    public static final SoundEvent GM94_RELOAD = get("gm94_reload");
    public static final SoundEvent SW1911_SHOOT = get("sw1911_shoot");
    public static final SoundEvent SW1911_RELOAD = get("sw1911_reload");
    public static final SoundEvent PYTHON357_SHOOT = get("python357_shoot");
    public static final SoundEvent PYTHON357_RELOAD = get("python357_reload");

    public static final SoundEvent BLOCK_BOOSTS_PLAYER = get("block_boosts_player");

    private static SoundEvent get(String name)
    {
        return new SoundEvent(new ResourceLocation(AVA.MODID, name)).setRegistryName(AVA.MODID, name);
    }

    public static void registerAll(IForgeRegistry<SoundEvent> registry)
    {
        registry.registerAll(
                DEFAULT_AIM,
                DEFAULT_PULL_GRENADE,
                DEFAULT_FOOTSTEP,

                NIGHT_VISION_ACTIVATE,
                UAV_CAPTURES,
                UAV_CAPTURED,

                AMMO_SUPPLIER_CONSUME,

                FOOTSTEP_WOOD,
                FOOTSTEP_METAL,
                FOOTSTEP_SAND,
                FOOTSTEP_SOLID,
                FOOTSTEP_GRASS,
                FOOTSTEP_FLOOD,
                FOOTSTEP_WOOL,

                GRENADE_HIT,

                COLLECTS_PICKABLE,

                BULLET_FLIES_BY,

                GENERIC_GRENADE_EXPLODE,
                FLASH_GRENADE_EXPLODE,
                ROCKET_EXPLODE,
                ROCKET_TRAVEL,
                SMOKE_GRENADE_ACTIVE,

                M4A1_SHOOT,
                M4A1_RELOAD,
                MOSIN_NAGANT_SHOOT,
                MOSIN_NAGANT_RELOAD,
                P226_SHOOT,
                P226_RELOAD,
                X95R_SHOOT,
                X95R_RELOAD,
                MK20_SHOOT,
                MK20_RELOAD,
                M24_SHOOT,
                M24_RELOAD,
                REMINGTON870_SHOOT,
                REMINGTON870_RELOAD,
                FN_FNC_SHOOT,
                FN_FNC_RELOAD,
                XM8_SHOOT,
                XM8_RELOAD,
                MP5SD5_SHOOT,
                MP5SD5_RELOAD,
                MK18_SHOOT,
                MK18_RELOAD,
                SR_25_SHOOT,
                SR_25_RELOAD,
                FG42_SHOOT,
                FG42_RELOAD,
                MP5K_SHOOT,
                MP5K_RELOAD,
                M202_SHOOT,
                M202_RELOAD,
                GM94_SHOOT,
                GM94_RELOAD,
                SW1911_SHOOT,
                SW1911_RELOAD,
                PYTHON357_SHOOT,
                PYTHON357_RELOAD,

                BLOCK_BOOSTS_PLAYER
        );
    }
}
