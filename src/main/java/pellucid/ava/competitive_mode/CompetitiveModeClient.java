package pellucid.ava.competitive_mode;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ClientPlayerNetworkEvent;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import pellucid.ava.AVA;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.packets.AVAPackets;
import pellucid.ava.misc.packets.PresetMessage;

import static pellucid.ava.misc.config.AVAClientConfig.*;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class CompetitiveModeClient
{
    @SubscribeEvent
    public static void playerRespawn(ClientPlayerNetworkEvent.RespawnEvent event)
    {
        int preset = PRESET_CHOICE.get();
        String p, se, m, p1, p2, p3, sp;
        if (preset == 1)
        {
            p = getOrDefaultEquipment(PRIMARY_WEAPON_1, AVAWeaponUtil.Classification.fromSlot(0));
            se = getOrDefaultEquipment(SECONDARY_WEAPON_1, AVAWeaponUtil.Classification.fromSlot(1));
            m = getOrDefaultEquipment(MELEE_WEAPON_1, AVAWeaponUtil.Classification.fromSlot(2));
            p1 = getOrDefaultEquipment(PROJECTILE_1_1, AVAWeaponUtil.Classification.fromSlot(3));
            p2 = getOrDefaultEquipment(PROJECTILE_1_2, AVAWeaponUtil.Classification.fromSlot(3));
            p3 = getOrDefaultEquipment(PROJECTILE_1_3, AVAWeaponUtil.Classification.fromSlot(3));
            sp = getOrDefaultEquipment(SPECIAL_WEAPON_1, AVAWeaponUtil.Classification.fromSlot(4));
        }
        else if (preset == 2)
        {
            p = getOrDefaultEquipment(PRIMARY_WEAPON_2, AVAWeaponUtil.Classification.fromSlot(0));
            se = getOrDefaultEquipment(SECONDARY_WEAPON_2, AVAWeaponUtil.Classification.fromSlot(1));
            m = getOrDefaultEquipment(MELEE_WEAPON_2, AVAWeaponUtil.Classification.fromSlot(2));
            p1 = getOrDefaultEquipment(PROJECTILE_2_1, AVAWeaponUtil.Classification.fromSlot(3));
            p2 = getOrDefaultEquipment(PROJECTILE_2_2, AVAWeaponUtil.Classification.fromSlot(3));
            p3 = getOrDefaultEquipment(PROJECTILE_2_3, AVAWeaponUtil.Classification.fromSlot(3));
            sp = getOrDefaultEquipment(SPECIAL_WEAPON_2, AVAWeaponUtil.Classification.fromSlot(4));
        }
        else
        {
            p = getOrDefaultEquipment(PRIMARY_WEAPON_3, AVAWeaponUtil.Classification.fromSlot(0));
            se = getOrDefaultEquipment(SECONDARY_WEAPON_3, AVAWeaponUtil.Classification.fromSlot(1));
            m = getOrDefaultEquipment(MELEE_WEAPON_3, AVAWeaponUtil.Classification.fromSlot(2));
            p1 = getOrDefaultEquipment(PROJECTILE_3_1, AVAWeaponUtil.Classification.fromSlot(3));
            p2 = getOrDefaultEquipment(PROJECTILE_3_2, AVAWeaponUtil.Classification.fromSlot(3));
            p3 = getOrDefaultEquipment(PROJECTILE_3_3, AVAWeaponUtil.Classification.fromSlot(3));
            sp = getOrDefaultEquipment(SPECIAL_WEAPON_3, AVAWeaponUtil.Classification.fromSlot(4));
        }
        AVAPackets.INSTANCE.sendToServer(new PresetMessage(p, se, m, p1, p2, p3, sp));
    }

    private static String getOrDefaultEquipment(ForgeConfigSpec.ConfigValue<String> value, AVAWeaponUtil.Classification classification)
    {
        Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(AVA.MODID, value.get()));
        if (item != null)
            return item.getRegistryName().toString();
        item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(AVA.MODID, classification.getDefaultFor()));
        return item == null ? Items.AIR.getRegistryName().toString() : item.getRegistryName().toString();
    }
}
